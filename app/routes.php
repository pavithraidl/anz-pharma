<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array(
    'as' => 'home',
    'uses' => 'HomeController@getHome'
));

Route::get('/about-us', array(
    'as' => 'about-us',
    'uses' => 'HomeController@getAboutUs'
));

Route::get('/contact', array(
    'as' => 'contact',
    'uses' => 'HomeController@getContact'
));

Route::get('/products/brands', array(
    'as' => 'products-brands',
    'uses' => 'HomeController@getBrands'
));

Route::get('/products/brand/{manufactureId}', array(
    'as' => 'products-manufacture',
    'uses' => 'HomeController@getManufactureGridView'
));

Route::get('/products/category/{categoryId}', array(
    'as' => 'products-category',
    'uses' => 'HomeController@getCategoryGridView'
));

Route::get('/products/list-view', array(
    'as' => 'products-list-view',
    'uses' => 'HomeController@getListView'
));

Route::get('/product/details/{productId}', array(
    'as' => 'products-details',
    'uses' => 'HomeController@getDetails'
));

Route::get('blog', array(
    'as' => 'blog',
    'uses' => 'HomeController@getBlog'
));

Route::get('blog-post', array(
    'as' => 'blog-post',
    'uses' => 'HomeController@getBlogPost'
));

Route::get('terms-of-use', array(
    'as' => 'terms-of-use',
    'uses' => 'HomeController@getTerms'
));


//API Routes
//--------------------------------------------------

Route::get('api/get-popular-categories', array(
    'as' => 'api-get-popular-categories',
    'uses' => 'ApiController@getPopularCategories'
));

Route::get('api/get-new-products', array(
    'as' => 'api-get-new-products',
    'uses' => 'ApiController@getNewProducts'
));

Route::get('api/get-featured-products', array(
    'as' => 'api-get-featured-products',
    'uses' => 'ApiController@getFeaturedProducts'
));

Route::get('api/get-product-details', array(
    'as' => 'api-get-product-details',
    'uses' => 'ApiController@getProductDetails'
));

Route::get('api/get-manufacture-products', array(
    'as' => 'api-get-manufacture-products',
    'uses' => 'ApiController@getManufactureProducts'
));

Route::get('api/get-manufactures', array(
    'as' => 'api-get-manufactures',
    'uses' => 'ApiController@getManufactures'
));

Route::get('api/get-category-products', array(
    'as' => 'api-get-category-products',
    'uses' => 'ApiController@getCategoryProducts'
));

