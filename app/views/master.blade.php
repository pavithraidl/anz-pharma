<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/03/18 09:06.
 */
?>

<!DOCTYPE html>
<html class="no-js" lang="">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>ANZ Pharma | {{ $pageName }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,700,400italic,700italic&subset=latin,vietnamese">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    {{HTML::style('assets/css/bootstrap.css')}}
    {{HTML::style('assets/css/plugins.css')}}
    {{HTML::style('assets/css/styles.css')}}
    {{HTML::style('assets/css/common.css')}}

    {{HTML::script('assets/js/vendor.js')}}

    {{HTML::script('assets/js/jquery.elevatezoom.js')}}

    @yield('styles')

    <link rel="icon" href="{{ URL::To('/') }}/assets/images/favicon.png" type="image/x-icon">

    <script>
        window.SHOW_LOADING = false;
    </script>
</head>

<body>
@include('product-quick-view')
<!-- // LOADING -->
<div class="awe-page-loading">
    <div class="awe-loading-wrapper">
        <div class="awe-loading-icon">
            <img src="{{ URL::To('/') }}/assets/images/short-logo.png" alt="ANZ Pharma Website Loading Icon" />
        </div>

        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
    </div>
</div>
<!-- // END LOADING -->

<div id="wrapper" class="main-wrapper ">
    <header id="header" class="awe-menubar-header">
        <div class="header-top">
            <div class="container">
                <div class="header-top-relative">

                    <nav class="navtop">
                        <div class="awe-logo">
                            <a href="{{ URL::Route('home') }}" title="ANZ Pharma Home">
                                <img src="{{ URL::To('/') }}/assets/images/logo.png" alt="ANZ Pharma Logo">
                            </a>
                        </div>
                        <!-- /.awe-logo -->

                        <ul class="navbar-icons">

                            <li class="menubar-account">
                                <a href="#" title="" class="awemenu-icon">
                                    <i class="icon icon-user-circle"></i>
                                    <span class="awe-hidden-text">Account</span>
                                </a>

                                <ul class="submenu megamenu">
                                    <li>
                                        <div class="container-fluid">
                                            <div class="header-account">
                                                <div class="header-account-avatar">
                                                    <a href="#" title="">
                                                        <img src="{{URL::To('/')}}/assets/images/icons/user_avatar.jpg" alt="ANZ Pharma User Avatar" class="img-circle">
                                                    </a>
                                                </div>

                                                <div class="header-account-username">
                                                    <h4><a href="#">Isuru Liyanage</a></h4>
                                                </div>

                                                <ul>
                                                    <li><a href="#">History</a>
                                                    </li>
                                                    <li><a href="#">Account Infomation</a>
                                                    </li>
                                                    <li><a href="#">Logout</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                            <li class="menubar-wishlist">
                                <a href="#" title="" class="awemenu-icon">
                                    <i class="glyphicon glyphicon-heart"></i>
                                    <span class="awe-hidden-text">Wishlist</span>
                                </a>

                                <ul class="submenu megamenu">
                                    <li>
                                        <div class="container-fluid">
                                            <ul class="whishlist">

                                                <li>
                                                    <div class="whishlist-item">
                                                        <div class="product-image">
                                                            <a href="#" title="">
                                                                <img src="{{URL::To('/')}}/assets/images/products/1.jpg" alt="">
                                                            </a>
                                                        </div>

                                                        <div class="product-body">
                                                            <div class="whishlist-name">
                                                                <h3><a href="#" title="">Lorem Ispuem</a></h3>
                                                            </div>

                                                            <div class="whishlist-price">
                                                                <span>Price:</span>
                                                                <strong>$12</strong>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <a href="#" title="" class="remove">
                                                        <i class="icon icon-remove"></i>
                                                    </a>
                                                </li>

                                                <li>
                                                    <div class="whishlist-item">
                                                        <div class="product-image">
                                                            <a href="#" title="">
                                                                <img src="{{URL::To('/')}}/assets/images/products/3.jpg" alt="">
                                                            </a>
                                                        </div>

                                                        <div class="product-body">
                                                            <div class="whishlist-name">
                                                                <h3><a href="#" title="">Lorem Ispeum</a></h3>
                                                            </div>

                                                            <div class="whishlist-price">
                                                                <span>Price:</span>
                                                                <strong>$15</strong>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <a href="#" title="" class="remove">
                                                        <i class="icon icon-remove"></i>
                                                    </a>
                                                </li>
                                            </ul>

                                            <hr>

                                            <div class="whishlist-action">
                                                <a href="#" title="" class="btn btn-dark btn-lg btn-outline btn-block">View Wishlist</a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                            <li class="menubar-cart">
                                <a href="#" title="" class="awemenu-icon menu-shopping-cart">
                                    <i class="icon icon-shopping-bag"></i>
                                    <span class="awe-hidden-text">Cart</span>

                                    <span class="cart-number" style="background: url('{{URL::To('/')}}/assets/images/backgrounds/bg-cart-number.png') center center no-repeat;">2</span>
                                </a>

                                <ul class="submenu megamenu">
                                    <li>
                                        <div class="container-fluid">

                                            <ul class="whishlist">

                                                <li>
                                                    <div class="whishlist-item">
                                                        <div class="product-image">
                                                            <a href="#" title="">
                                                                <img src="{{URL::To('/')}}/assets/images/products/2.jpg" alt="">
                                                            </a>
                                                        </div>

                                                        <div class="product-body">
                                                            <div class="whishlist-name">
                                                                <h3><a href="#" title="">Lorem Ispuem</a></h3>
                                                            </div>

                                                            <div class="whishlist-price">
                                                                <span>Price:</span>
                                                                <strong>$12</strong>
                                                            </div>

                                                            <div class="whishlist-quantity">
                                                                <span>Quantity:</span>
                                                                <span>1</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <a href="#" title="" class="remove">
                                                        <i class="icon icon-remove"></i>
                                                    </a>
                                                </li>

                                                <li>
                                                    <div class="whishlist-item">
                                                        <div class="product-image">
                                                            <a href="#" title="">
                                                                <img src="{{URL::To('/')}}/assets/images/products/4.jpg" alt="">
                                                            </a>
                                                        </div>

                                                        <div class="product-body">
                                                            <div class="whishlist-name">
                                                                <h3><a href="#" title="">Lorem Ispuem</a></h3>
                                                            </div>

                                                            <div class="whishlist-price">
                                                                <span>Price:</span>
                                                                <strong>$14</strong>
                                                            </div>

                                                            <div class="whishlist-quantity">
                                                                <span>Quantity:</span>
                                                                <span>3</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <a href="#" title="" class="remove">
                                                        <i class="icon icon-remove"></i>
                                                    </a>
                                                </li>

                                            </ul>

                                            <div class="menu-cart-total">
                                                <span>Total</span>
                                                <span class="price">$54</span>
                                            </div>

                                            <div class="cart-action">
                                                <a href="#" title="" class="btn btn-lg btn-dark btn-outline btn-block">View cart</a>
                                                <a href="#" title="" class="btn btn-lg btn-primary btn-block">Proceed To Checkout</a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>


                        </ul>

                        <div class="policy-header">
                            <div class="policy">
                                <a href="#">
                                    <div class="policy-icon">
                                        <i class="icon icon-dolar-circle"></i>
                                    </div>

                                    <div class="policy-text">
                                        <h4>Daily Deals</h4>
                                        <p>Offers</p>
                                    </div>
                                </a>
                            </div>
                            <!-- /.policy -->

                            <div class="policy">
                                <a href="#">
                                    <div class="policy-icon">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                    </div>

                                    <div class="policy-text">
                                        <h4>Blog</h4>
                                        <p>News & Updates</p>
                                    </div>
                                </a>
                            </div>
                            <!-- /.policy -->

                            <div class="policy">
                                <a class="@if($pageName == 'About Us') active @endif" href="#">
                                    <div class="policy-icon">
                                        <i class="fa fa-group"></i>
                                    </div>

                                    <div class="policy-text">
                                        <h4>About Us</h4>
                                        <p>Who We Are</p>
                                    </div>
                                </a>
                            </div>
                            <!-- /.policy -->
                        </div>

                    </nav>
                </div>
            </div>
        </div>
        <nav id="navbar" class="awemenu-nav" data-responsive-width="1200">
            <div class="container">
                <div class="awemenu-container">

                    <ul class="navbar-search">
                        <li>
                            <a href="#" title="" class="awemenu-icon awe-menubar-search" id="open-search-form" style="width: 100% !important;">
                                <span class="sr-only">Search</span>
                                <span class="icon icon-search"></span>
                            </a>

                            <div class="menubar-search-form" id="menubar-search-form">
                                <form action="{{URL::To('/')}}" method="GET">
                                    <input type="text" name="s" class="form-control" placeholder="Search your entry here...">
                                    <div class="menubar-search-buttons">
                                        <button type="submit" class="btn btn-sm btn-white">Search</button>
                                        <button type="button" class="btn btn-sm btn-white" id="close-search-form">
                                            <span class="icon icon-remove"></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.menubar-search-form -->
                        </li>
                    </ul>

                    <ul class="awemenu awemenu-left">
                        <li class="awemenu-item @if($pageName == 'Home') active @endif"><a href="{{ URL::Route('home') }}"> <i class="fa fa-home" style="font-size: 24px;"></i> </a></li>
                        @foreach($menu as $menuItem)
                            <li class="awemenu-item">
                                <a href="#" title="">
                                    <span>{{ $menuItem['name'] }}</span>
                                </a>

                                <ul class="awemenu-submenu awemenu-megamenu" data-width="100%" data-animation="fadeup">
                                    <li class="awemenu-megamenu-item">
                                        <div class="container-fluid">
                                            <div class="awemenu-megamenu-wrapper">

                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <h2 class="upper">{{ $menuItem['name'] }}</h2>

                                                        <ul class="super">
                                                            <li><a href="#" title="">Bestseller</a>
                                                            </li>
                                                            <li><a href="#" title="">New Arrivals</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="row same-height">
                                                            @if(sizeof($menuItem['category']) > 0)
                                                                @foreach($menuItem['category'] as $category)
                                                                    <div class="col-lg-4" style="margin-bottom: 20px;">
                                                                        <a href="{{ URL::To('/') }}/products/category/{{ $category['id'] }}">
                                                                            <h5>{{ $category['name'] }}</h5>
                                                                        </a>
                                                                        <ul class="sublist">
                                                                            @foreach($category['subcategory'] as $subCategory)
                                                                                <li><a href="{{ URL::To('/') }}/products/category/{{ $subCategory['id'] }}" title="" style="margin-top: -10px;">{{ $subCategory['name'] }}</a></li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </div>
                                                                @endforeach
                                                            @else
                                                                <p style="text-align: center;">No Categories. <a href="#">Shop All</a> </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 pull-right">
                                                        <div class="awe-media inline margin-bottom-25">
                                                            <div class="awe-media-image">
                                                                <a href="#" title="">
                                                                    @if($menuItem['img'] == 1)
                                                                        <img src="{{ VariableController::server }}assets/backend/images/website/menu/{{ VariableController::orgId }}/{{ $menuItem['id'] }}.{{ $menuItem['ext'] }}" alt="">
                                                                    @endif
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="bottom-link">
                                                    <a href="#" class="btn btn-lg btn-dark btn-outline">
                                                        <span>Shop All</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        @endforeach
                        <li class="awemenu-item @if($pageName == 'Brands') active @endif"><a href="{{ URL::Route('products-brands') }}"> Brands </a></li>
                    </ul>
                </div>
            </div>
            <!-- /.container -->

        </nav>
        <!-- /.awe-menubar -->
    </header>
    <!-- /.awe-menubar-header -->

    <div id="main">

        {{-- Content Start--}}
        {{----------------------------------}}
            @yield('content')
        {{----------------------------------}}
        {{-- Content End --}}

    </div>


    <footer class="footer">
        <div class="footer-wrapper">
            <div class="footer-widgets">


                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12 col-sm-6">

                                    <div class="widget">
                                        <img src="{{ URL::To('/') }}/assets/images/footer-logo.png" style="max-width: 250px;"/>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6">

                            <div class="widget">
                                <a href="#">
                                    <h3 class="widget-title">Address</h3>

                                    <p>{{ $contentObject['56']['102']['address'] }}</p>
                                </a>
                            </div>
                            <!-- /.widget -->

                        </div>

                        <div class="col-md-3 col-sm-6">

                            <div class="widget">
                                <h3 class="widget-title">Contact Details</h3>

                                <p>
                                    <a href="tel:{{ $contentObject['56']['102']['phone'] }}"> PHONE: {{ $contentObject['56']['102']['phone'] }}</a><br/>
                                    <a href="fax:{{ $contentObject['56']['102']['fax'] }}"> FAX: {{ $contentObject['56']['102']['fax'] }}</a><br/>
                                    <a href="mailto:{{ $contentObject['56']['102']['email'] }}"> EMAIL: {{ $contentObject['56']['102']['email'] }}</a>
                                </p>
                            </div>
                            <!-- /.widget -->

                        </div>

                        <div class="col-md-3">

                            <div class="widget">
                                <h3 class="widget-title">Be in touch</h3>

                                <ul class="list-socials">
                                    @if($contentObject['56']['102']['facebook_link'] != '' && $contentObject['56']['102']['facebook_link'] != '#')
                                        <li><a href="{{ $contentObject['56']['102']['facebook_link'] }}" title=""><i class="icon icon-facebook"></i></a></li>
                                    @endif
                                    @if($contentObject['56']['102']['instagram_link'] != '' && $contentObject['56']['102']['facebook_link'] != '#')
                                        <li><a href="{{ $contentObject['56']['102']['instagram_link'] }}" title=""><i class="fa fa-instagram"></i></a></li>
                                    @endif
                                    @if($contentObject['56']['102']['google+_link'] != '' && $contentObject['56']['102']['facebook_link'] != '#')
                                        <li><a href="{{ $contentObject['56']['102']['google+_link'] }}" title=""><i class="icon icon-google-plus"></i></a></li>
                                    @endif
                                    @if($contentObject['56']['102']['twitter_link'] != '' && $contentObject['56']['102']['facebook_link'] != '#')
                                        <li><a href="{{ $contentObject['56']['102']['twitter_link'] }}" title=""><i class="icon icon-twitter"></i></a></li>
                                    @endif
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
            <!-- /.footer-widgets -->

            <div class="footer-copyright">
                <div class="container">
                    <div class="copyright">
                        <p style="color: #9b9b9b !important;">Copyright &copy; 2018 ANZ Pharma - All Rights Reserved. Powered by <a href="https://prisu.nz" style="color: #cdcdcd;">GoPlaza</a> </p>
                    </div>

                    <div class="footer-nav">
                        <nav>
                            <ul>
                                <li class="@if($pageName == 'About Us') active @endif"><a href="#" title="">About Us</a>
                                </li>
                                <li class="@if($pageName == 'Contacts') active @endif"><a href="#" title="">Contacts</a>
                                </li>
                                <li><a href="index2.html#" title="">Term of Use</a>
                                </li>
                            </ul>
                        </nav>

                        <nav>
                            <ul>
                                <li><i class="fa fa-cc-mastercard"></i> </li>
                                <li><i class="fa fa-cc-visa"></i> </li>
                                <li><i class="fa fa-cc-paypal"></i> </li>
                                <li><i class="fa fa-cc-discover"></i> </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- /.footer-copyright -->
        </div>
        <!-- /.footer-wrapper -->

        <a href="index2.html#" class="back-top" title="">
                <span class="back-top-image">
                    <img src="{{ URL::To('/') }}/assets/images/backgrounds/back-top.png" alt="">
                </span>

            <small>Back top top</small>
        </a>
        <!-- /.back-top -->
    </footer>
    <!-- /footer -->

</div>
<!-- /#wrapper -->


<div id="login-popup" class="white-popup login-popup mfp-hide">
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="index2.html#account-login" aria-controls="account-login" role="tab" data-toggle="tab">Account Login</a>
            </li>

            <li role="presentation">
                <a href="index2.html#account-register" aria-controls="account-register" role="tab" data-toggle="tab">Register</a>
            </li>
        </ul>
        <!-- /.nav -->

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="account-login">
                <form action="index.html" method="POST">
                    <div class="form-group">
                        <label for="login-account">Account</label>
                        <input type="text" class="form-control" id="login-account">
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label for="login-password">Password</label>
                        <input type="password" class="form-control" id="login-password" data-show-password="true">
                    </div>
                    <!-- /.form-group -->

                    <div class="forgot-passwd">
                        <a href="index2.html#" title="">
                            <i class="icon icon-key"></i>
                            <span>Forgot your password?</span>
                        </a>
                    </div>
                    <!-- /.forgot-passwd -->

                    <div class="form-button">
                        <button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->

            <div role="tabpanel" class="tab-pane" id="account-register">
                <form action="index.html" method="POST">
                    <div class="form-group">
                        <label for="register-username">Username <sup>*</sup>
                        </label>
                        <input type="text" class="form-control" id="register-username">
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label for="register-email">Email <sup>*</sup>
                        </label>
                        <input type="text" class="form-control" id="register-email">
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label for="register-password">Password <sup>*</sup>
                        </label>
                        <input type="password" class="form-control" id="register-password">
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label for="register-confirm-password">Confirm Password <sup>*</sup>
                        </label>
                        <input type="password" class="form-control" id="register-confirm-password">
                    </div>
                    <!-- /.form-group -->

                    <div class="form-button">
                        <button type="submit" class="btn btn-lg btn-warning btn-block">Register</button>
                    </div>
                    <!-- /.form-button -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
</div>
<!-- /.login-popup -->
<script>
    $(function() {
        $('a[href="#login-popup"]').magnificPopup({
            type:'inline',
            midClick: false,
            closeOnBgClick: false
        });
    });

    $(document).ready(function () {
        window.domain = '{{ URL::To('/') }}/';
        window.server = '{{ VariableController::server }}';
        window.orgId = '{{ VariableController::orgId }}';
    })
</script>

{{HTML::script('assets/js/plugins.js')}}

{{HTML::script('assets/js/main.js')}}

{{HTML::script('assets/js/docs.js')}}

{{HTML::script('assets/js/quick-view.js')}}

@yield('scripts')

</body>

</html>

