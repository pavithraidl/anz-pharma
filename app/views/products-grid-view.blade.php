<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/03/18 10:22.
 */
?>

@extends('master')

@section('styles')
    {{HTML::style('assets/css/grid-view.css')}}
@endsection

@section('content')

    <div class="main-header background background-image-heading-products" style="background: url('{{ $pageData->banner_imgurl }}');">
        <div class="container">
            <?php
                if($pageData->title_color == '' || $pageData->title_color == null) {
                    $titleColor = '#00b4af';
                }
                else {
                    $titleColor = $pageData->title_color;
                }
            ?>
            <h1 style="color: {{ $titleColor }}; text-align: center;">{{ $pageData->name }}</h1>
        </div>
    </div>


    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ URL::Route('home') }}">Home</a></li>
                {{ $pageData->breadcrumb }}
            </ol>

        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-3">

                <div class="product-header-actions">
                    <form action="#" method="POST" class="form-inline">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="view-icons">
                                    <a href="#" class="view-icon active"><span class="icon icon-th"></span></a>
                                    <a href="#" class="view-icon "><span class="icon icon-th-list"></span></a>
                                </div>

                                <div class="view-count">
                                    <span id="item-count" class="text-muted">Item 0 to 0 of loading Items</span>
                                </div>
                            </div>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <div class="form-show-sort">
                                    <div class="form-group pull-left">

                                    </div>
                                    <!-- /.form-group -->

                                    <div class="form-group pull-right text-right">
                                        <label for="p_sort_by">Sort By</label>
                                        <select onchange="sortProducts();" name="p_sort_by" id="p_sort_by" class="form-control input-sm">
                                            <option value="1">Popular</option>
                                            <option value="2">Price (Low - High)</option>
                                            <option value="3">Price (High - Low)</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </form>
                </div>
                <!-- /.product-header-actions -->


                <div class="products products-grid-wrapper" style="position: relative;">
                    <div id="product-container" class="row loading">
                        {{-- JQuery Append Products --}}
                    </div>
                    <div class="loader"></div>
                    <!-- /.row -->
                </div>
                <!-- /.products -->

            </div>
            <!-- /.col-* -->

            <div class="col-md-3 col-md-pull-9">
                <div id="shop-widgets-filters" class="shop-widgets-filters">

                    <div id="widget-area" class="widget-area">

                        <div class="widget woocommerce widget_product_categories">
                            <h3 class="widget-title">Categories</h3>

                            <ul>
                                <li class="active"><a href="#" title="" style="font-weight: 600;">All Products</a>
                                </li>
                                <li><a href="#" title="">For Baby & Child &nbsp;<i class="fa fa-angle-down"></i> </a>
                                    <ul style="margin-left: 25px; font-size: 12px;">
                                        <li><a href="#">Baby Bath</a></li>
                                        <li><a href="#">Baby Lotion & Oils </a></li>
                                        <li><a href="#">Baby Powder </a></li>
                                        <li><a href="#">Baby Nappies </a></li>
                                        <li><a href="#">Baby Wipes </a></li>
                                        <li><a href="#">Nutrition Supplements </a></li>
                                    </ul>
                                </li>
                                <li><a href="#" title="">For Mom &nbsp;<i class="fa fa-angle-up"></i> </a>
                                </li>
                                <li><a href="#" title="">Accessories &nbsp;<i class="fa fa-angle-up"></i> </a>
                                </li>

                            </ul>
                        </div>
                        <!-- /.widget -->

                        @if($pageType != 1)
                        <div class="widget">
                            <h3 class="widget-title">Brands</h3>

                            <div class="widget-content">
                                <div class="awewoo-brand">
                                    <div class="awewoo-brand-header">
                                        <input type="text" class="form-control" placeholder="Find your brand">
                                    </div>

                                    <div class="awewoo-brand-content">
                                        <div class="nano" style="max-height: 150px;">
                                            <div class="nano-content">
                                                <ul>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Synxsole</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Bobble</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Burnaid</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Double "D"</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Moxie</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>RID</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Gluco</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Hood</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Kill City</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Police</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Vans</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Hood</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Kill City</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Police</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Vans</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Hood</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Kill City</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Police</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                    </div>

                </div>

                <div id="open-filters">
                    <i class="fa fa-filter"></i>
                    <span>Filter</span>
                </div>
            </div>
            <!-- /.col-* -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

@endsection

@section('scripts')
    {{ HTML::script('assets/js/grid-view.js') }}

    <script>
        window.pageType = {{ $pageType }};
        window.pageId = {{ $pageId }};
    </script>
@endsection
