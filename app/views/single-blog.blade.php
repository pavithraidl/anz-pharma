<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 19/03/18 16:31.
 */
?>

@extends('master')

@section('content')

    <div class="main-header background background-image-heading-blog">
        <div class="container">
            <h1>Blog Details</h1>
        </div>
    </div>


    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="blog-detail.html#">Home</a>
                </li>
                <li class="active"><span>Blog Details</span>
                </li>
            </ol>

        </div>
    </div>


    <div class="blog-wrapper blog-detail">
        <div class="container">
            <div class="row">

                <div class="col-md-9 col-md-push-3">
                    <article class="post">

                        <div class="entry-media">
                            <div class="entry-feature-image">
                                <img src="{{ URL::To('/') }}/html/images/posts/sidebar/1.jpg" alt="">
                            </div>
                        </div>
                        <!-- ./entry-media -->

                        <div class="entry-container">
                            <header class="entry-header">
                                <div class="entry-datetime">
                                    <span class="entry-day">28</span>
                                    <span class="entry-month">/June</span>
                                </div>

                                <h1 class="entry-title">Help The Body Run More Beautiful When Wearing</h1>

                                <div class="entry-meta">
                                    <span>Posted by</span>
                                    <strong class="entry-author"><a href="blog-detail.html#" title="">Join Doe </a></strong>
                                    <span>-</span>
                                    <span class="entry-category"><a href="blog-detail.html#" title="">Clothing</a>  </span>
                                    <span>-</span>
                                    <span class="entry-comment-count"><a href="blog-detail.html#" title="">3 Comments</a></span>

                                    <p class="entry-tags">
                                        <span>/ Tags:</span>
                                        <a href="blog-detail.html#">Accessories</a>,
                                        <a href="blog-detail.html#">Clothing</a>,
                                        <a href="blog-detail.html#">Summer 2015</a>,
                                        <a href="blog-detail.html#">Trending</a>
                                    </p>
                                </div>

                            </header>
                            <!-- /.entry-header -->

                            <div class="entry-content">
                                <p>Etiam vitae ipsum magna. Aliquam a elit quis orci tincidunt consectetur. Vivamus et nibh ut dolor lobortis gravida ut ut risus. Nam quis facilisis magna. Nunc imperdiet libero sapien, ut faucibus magna pharetra
                                    a. Mauris id comodo dolor. Donec pellentesque semper velit vel tempor. Pellentesque in erat vitae libero lobortis malesuada id eget nulla. Aliquam eleifend diam non nisi tincidunt, eu auctor ligula sagittis.
                                    Aliquam lacus massa, sodales ut vehicula a, interdum eget urna.</p>

                                <!-- Blockquote -->
                                <blockquote>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                </blockquote>

                                <!-- Image width caption -->
                                <figure class="wp-caption aligncenter">
                                    <img src="http://lorempixel.com/800/400" alt="">
                                    <figcaption class="wp-caption-text">Here's a sample caption with an image centered.</figcaption>
                                </figure>

                                <p>Donec pellentesque semper Vivamus et nibh ut dolor lobortis gravida ut ut risus. Nam quis facilisis magna. Nunc imperdiet libero sapien, ut faucibus magna pharetra a. Mauris id commodo dolor. Donec pellentesque
                                    semper.</p>

                                <p class="text-muted">** Duis ut massa in sem dapibus dignissim. In semper at leo in consectetur. In sit amet mattis metus. Cras in auctor orci. Etiam vitae ipsum magna. Nunc imperdiet libero sapien, ut faucibus magna pharetra a. Mauris
                                    id commodo dolor. Donec pellentesque semper</p>

                            </div>
                            <!-- /.entry-content -->

                            <footer class="entry-footer">
                                <div class="entry-share">
                                            <span class="title">
                                    <span class="margin-right-10">
                                        <i class="fa fa-share-alt"></i>
                                    </span>
                                            <span>Share:</span>
                                            </span>

                                    <ul class="list-socials">
                                        <li><a href="blog-detail.html#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li><a href="blog-detail.html#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li><a href="blog-detail.html#"><i class="fa fa-google-plus"></i></a>
                                        </li>
                                        <li><a href="blog-detail.html#"><i class="fa fa-vine"></i></a>
                                        </li>
                                        <li><a href="blog-detail.html#"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.entry-share -->
                            </footer>
                            <!-- /.entry-footer -->
                        </div>
                        <!-- /.entry-container -->
                    </article>
                    <!-- /.post -->

                    <div class="entry-container">

                        <div id="comments" class="comments-area">
                            <h2 class="comment-title">Comment (3)</h2>

                            <div class="comments-list">
                                <ol>
                                    <li>
                                        <div class="comment">
                                            <a class="comment-avatar" href="blog-detail.html#">
                                                <img class="media-object" src="{{ URL::To('/') }}/html/images/avatars/members/1.jpg" alt="">
                                            </a>

                                            <div class="comment-body">
                                                <h4 class="comment-title">1914 translation by H. Rackham</h4>
                                                <p class="comment-text">Vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupidi</p>
                                                <div class="comment-meta">
                                                    <a href="blog-detail.html#" title="" class="reply-comment-action">Reply</a>

                                                </div>
                                            </div>
                                        </div>

                                        <ol>
                                            <li>
                                                <div class="comment">
                                                    <a class="comment-avatar" href="blog-detail.html#">
                                                        <img class="media-object" src="{{ URL::To('/') }}/html/images/avatars/members/1.jpg" alt="">
                                                    </a>

                                                    <div class="comment-body">
                                                        <h4 class="comment-title">1914 translation by H. Rackham</h4>
                                                        <p class="comment-text">Vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupidi</p>
                                                        <div class="comment-meta">
                                                            <a href="blog-detail.html#" title="" class="reply-comment-action">Reply</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <ol>
                                                    <li>
                                                        <div class="comment">
                                                            <a class="comment-avatar" href="blog-detail.html#">
                                                                <img class="media-object" src="{{ URL::To('/') }}/html/images/avatars/members/1.jpg" alt="">
                                                            </a>

                                                            <div class="comment-body">
                                                                <h4 class="comment-title">1914 translation by H. Rackham</h4>
                                                                <p class="comment-text">Vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupidi</p>
                                                                <div class="reply">
                                                                    <a href="blog-detail.html#" title="" class="reply-comment-action">Reply</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ol>
                                            </li>
                                        </ol>
                                    </li>

                                    <li>
                                        <div class="comment">
                                            <a class="comment-avatar" href="blog-detail.html#">
                                                <img class="media-object" src="{{ URL::To('/') }}/html/images/avatars/members/1.jpg" alt="">
                                            </a>

                                            <div class="comment-body">
                                                <h4 class="comment-title">1914 translation by H. Rackham</h4>
                                                <p class="comment-text">Vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupidi</p>
                                                <div class="reply">
                                                    <a href="blog-detail.html#" title="" class="reply-comment-action">Reply</a>
                                                    <a href="blog-detail.html#" title="" class="edit-comment-action">Edit</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ol>
                            </div>

                            <div class="reply-comment">
                                <h2 class="reply-comment-title">Leave a comment</h2>

                                <form action="blog-detail.html" method="POST">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="reply-name" class="sr-only">Name</label>
                                                <input type="text" class="form-control" id="reply-name" placeholder="Name">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="reply-email" class="sr-only">Email</label>
                                                <input type="email" class="form-control" id="reply-email" placeholder="Email">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="reply-website" class="sr-only">Website</label>
                                        <input type="text" class="form-control" id="reply-website" placeholder="Website">
                                    </div>

                                    <div class="form-group">
                                        <label for="reply-text" class="sr-only">Your comment</label>
                                        <textarea name="reply-text" class="form-control" id="reply-text" rows="7" placeholder="Your comment"></textarea>
                                    </div>

                                    <div class="form-submit">
                                        <button type="submit" class="btn btn-lg btn-dark btn-outline">Submit comment</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.entry-container -->
                </div>

                <div class="col-md-3 col-md-pull-9">

                    <div id="widget-area" class="widget-area">

                        <div class="widget widget_categories">
                            <h3 class="widget-title">Categories</h3>

                            <ul>
                                <li class="current"><a href="blog-detail.html#" title="">All</a>
                                </li>
                                <li><a href="blog-detail.html#" title="">Women</a>
                                </li>
                                <li><a href="blog-detail.html#" title="">Men</a>
                                </li>
                                <li><a href="blog-detail.html#" title="">Shoes</a>
                                </li>
                                <li><a href="blog-detail.html#" title="">Clothing</a>
                                </li>
                                <li><a href="blog-detail.html#" title="">Accessories</a>
                                </li>
                                <li><a href="blog-detail.html#" title="">Discount</a>
                                </li>
                                <li><a href="blog-detail.html#" title="">Hot Trend</a>
                                </li>
                            </ul>
                        </div>




                        <div class="widget widget_recent_comments">
                            <h3 class="widget-title">Comments</h3>

                            <ul>
                                <li>
                                    <div class="widget-comment">
                                        <h4 class="widget-comment-title">
                                            <a href="blog-detail.html#" title="">Goodale Rutledge</a>
                                        </h4>

                                        <div class="widget-comment-time">
                                            <a href="blog-detail.html#" title="">
                                                <span>February 17, 2015</span>
                                            </a>
                                        </div>

                                        <div class="widget-comment-sumary">
                                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.</p>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="widget-comment">
                                        <h4 class="widget-comment-title">
                                            <a href="blog-detail.html#" title="">Goodale Rutledge</a>
                                        </h4>

                                        <div class="widget-comment-time">
                                            <a href="blog-detail.html#" title="">
                                                <span>February 17, 2015</span>
                                            </a>
                                        </div>

                                        <div class="widget-comment-sumary">
                                            <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="widget-comment">
                                        <h4 class="widget-comment-title">
                                            <a href="blog-detail.html#" title="">Goodale Rutledge</a>
                                        </h4>

                                        <div class="widget-comment-time">
                                            <a href="blog-detail.html#" title="">
                                                <span>February 17, 2015</span>
                                            </a>
                                        </div>

                                        <div class="widget-comment-sumary">
                                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- /.widget -->




                        <div class="widget widget_recent_entries">
                            <h3 class="widget-title">Recent Posts</h3>

                            <ul>
                                <li><a href="blog-detail.html#" title="">New Jackets for Winter</a>
                                </li>
                                <li><a href="blog-detail.html#" title="">Hot Trending Clothing for Man</a>
                                </li>
                                <li><a href="blog-detail.html#" title="">New Arrivals Shoes for Girl</a>
                                </li>
                                <li><a href="blog-detail.html#" title="">Hot Trending Clothing for Man</a>
                                </li>
                                <li><a href="blog-detail.html#" title="">Up Sales to 50% OFF</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.widget -->




                        <div class="widget widget_tag_cloud">
                            <h3 class="widget-title">Tags</h3>

                            <div class="tagcloud">
                                <a href="blog-detail.html#" title="">Shoes</a>
                                <a href="blog-detail.html#" title="">Clothing</a>
                                <a href="blog-detail.html#" title="">Fashion</a>
                                <a href="blog-detail.html#" title="">Bags</a>
                                <a href="blog-detail.html#" title="">Accessories</a>
                                <a href="blog-detail.html#" title="">Discount</a>
                                <a href="blog-detail.html#" title="">Jackets</a>
                                <a href="blog-detail.html#" title="">Shorts</a>
                                <a href="blog-detail.html#" title="">Tees</a>
                            </div>
                        </div>
                        <!-- /.widget -->



                        <div class="widget">
                            <h3 class="widget-title">Socials</h3>

                            <ul class="list-socials">

                                <li><a href="blog-detail.html#" data-toggle="tooltip" title="Twitter"><i class="icon icon-twitter"></i></a>
                                </li>
                                <li><a href="blog-detail.html#" data-toggle="tooltip" title="Facebook"><i class="icon icon-facebook"></i></a>
                                </li>
                                <li><a href="blog-detail.html#" data-toggle="tooltip" title="Dot-Dot"><i class="icon icon-dot-dot"></i></a>
                                </li>
                                <li><a href="blog-detail.html#" data-toggle="tooltip" title="Google+"><i class="icon icon-google-plus"></i></a>
                                </li>
                                <li><a href="blog-detail.html#" data-toggle="tooltip" title="Pinterest"><i class="icon icon-pinterest"></i></a>
                                </li>

                            </ul>
                        </div>
                        <!-- /.widget -->



                        <div class="widget">
                            <h3 class="widget-title">Ads</h3>

                            <div class="widget-content">
                                <a href="blog-detail.html#">
                                    <img src="{{ URL::To('/') }}/html/images/ads/1.jpg" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- /.widget -->

                    </div>

                </div>

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.blog-wrapper -->    

@endsection
