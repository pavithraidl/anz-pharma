<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/03/18 09:08.
 */
?>

@extends('master')

@section('styles')
    {{HTML::style('assets/css/home-animations.css')}}
@endsection

@section('content')
    <section>
        <div class="main-slider-wrapper">
            <div class="main-slider owl-carousel owl-carousel-inset">

                @foreach($contentObject['46'] as $slide)
                    <div class="main-slider-item">
                        <div class="main-slider-image">
                            <img src="{{ $slide['slider_image_url'] }}" alt="">
                        </div>

                        <div class="main-slider-text">
                            <div class="fp-table">
                                <div class="fp-table-cell center">

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <script>
            $(function() {  aweMainSlider(); });
        </script>

    </section>
    <!-- /section -->

    <section>
        <div class="container">
            <div class="policy-wrapper policy-style">

                <div class="row">

                    <div class="col-md-4 col-sm-4 col-xs-8">
                        <div class="policy">
                            <div class="policy-icon">
                                <i class="fa fa-{{ $contentObject['48']['83']['block_icon'] }}"></i>
                            </div>

                            <div class="policy-text">
                                <h4>{{ $contentObject['48']['83']['top_text'] }}</h4>
                                <p>{{ $contentObject['48']['83']['bottom_text'] }}</p>
                            </div>
                        </div>
                        <!-- /.policy -->
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-8">
                        <div class="policy">
                            <div class="policy-icon">
                                <i class="fa fa-{{ $contentObject['48']['84']['block_icon'] }}"></i>
                            </div>

                            <div class="policy-text">
                                <h4>{{ $contentObject['48']['84']['top_text'] }}</h4>
                                <p>{{ $contentObject['48']['84']['bottom_text'] }}</p>
                            </div>
                        </div>
                        <!-- /.policy -->
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-8">
                        <div class="policy">
                            <a href="tel:{{ $contentObject['48']['85']['top_text'] }}">
                                <div class="policy-icon">
                                    <i class="fa fa-{{ $contentObject['48']['85']['block_icon'] }}"></i>
                                </div>

                                <div class="policy-text">
                                    <h4>{{ $contentObject['48']['85']['top_text'] }}</h4>
                                    <p style="font-size: 14px !important;">{{ $contentObject['48']['85']['top_text'] }}</p>
                                </div>
                            </a>
                        </div>
                        <!-- /.policy -->
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.policy-wrapper -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section>
        <div class="container">

            <div class="home-trending padding-vertical-50">
                <div class="section-header center">
                    <h2 class="upper margin-bottom-20">What's Popular</h2>
                    <p>Most popular these days</p>
                </div>
                <!-- ./section-header -->
                <div id="popular-categories-container-row" class="row loading">
                    <div class="col-md-12">
                        <div class="popular-categories-container">
                            {{-- JQuery Append --}}
                        </div>
                    </div>
                    <div class="show-more-container row">
                        <div class="col-md-12">
                            <p class="popular-category-show-more-container" style="text-align: center">
                                <span class="popular-category-show-more" onclick="showMoreProductCategories();">More <i class="fa fa-angle-double-down"></i> </span>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.home-trending -->

        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <div class="container">
        <div class="divider"></div>
    </div>

    <section>
        <div class="container">
            <div class="padding-vertical-50">

                <div class="arrivals">
                    <div class="section-header center">
                        <h2>New Arrivals</h2>
                    </div>
                    <!-- /.section-header -->

                    <div class="products scroll">
                        <div id="new-products-container-row" class="row loading">
                            <div class="col-md-12">
                                <div class="new-products-container">
                                    {{-- JQuery Append --}}
                                </div>
                            </div>
                            <div class="show-more-container row">
                                <div class="col-md-12">
                                    <p class="new-products-show-more-container" style="text-align: center">
                                        <span class="new-products-show-more" onclick="showMoreNewProducts();">More <i class="fa fa-angle-double-down"></i> </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.arrivals -->
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <div class="container">
        <div class="divider"></div>
    </div>

    <section>
        <div class="container">

            <div class="padding-vertical-50 border-bottom">
                <div class="section-header center">
                    <h2>Featured Items</h2>
                </div>
                <!-- /.section-header -->

                <div class="products scroll">
                    <div id="featured-products-container-row" class="row loading">
                        <div class="col-md-12">
                            <div class="featured-products-container">
                                {{-- JQuery Append --}}
                            </div>
                        </div>
                        <div class="show-more-container row">
                            <div class="col-md-12">
                                <p class="featured-products-show-more-container" style="text-align: center">
                                    <span class="featured-products-show-more" onclick="showMoreFeaturedProducts();">More <i class="fa fa-angle-double-down"></i> </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.padding-vertical-50 -->

        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section class="background background-color-dark background-image-section-customers-say" style="background-image: url('{{ URL::To('/') }}/assets/images/backgrounds/1.jpg')">
        <div class="container">
            <div class="padding-top-60">
                <div class="section-header center">
                    <h2>Customer Say</h2>
                </div>
                <!-- /.section-header -->

                <div class="section-customers">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="customers-carousel owl-carousel" id="customers-carousel" data-toggle="carousel" data-dots="true" data-nav="0">
                                @foreach($contentObject['49'] as $testimonial)
                                    <div class="center">
                                        <h4>{{ $testimonial['name'] }}</h4>
                                        <p>“{{ $testimonial['content'] }}“</p>
                                    </div>
                                    <!-- /.center -->
                                @endforeach
                            </div>
                            <!-- /.customers-say-carousel -->
                        </div>
                    </div>
                </div>
                <!-- /.section-content -->
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section>
        <div class="container">
            <div class="padding-vertical-60">
                <div class="section-header">
                    <h2>Blog</h2>
                </div>

                <div class="section-content">
                    <div class="home-section-posts owl-carousel">

                        <div class="section-post">
                            <div class="section-post-media">
                                <a href="#" title="San Francisco Felt Pennant">
                                    <img src="https://www.happy-or-not.com/content/uploads/2017/03/Pharmacy.jpg" alt="">
                                </a>
                            </div>

                            <div class="section-post-body">
                                <h4 class="section-post-title">
                                    <a href="#" title="San Francisco Felt Pennant">Lorem ipsum dolor sit Hair Care</a>
                                </h4>

                                <p class="section-post-excerpt">Lorem Ipsum is simply dummy text of the pharmacy and drugs industry.</p>

                                <a href="#" title="" class="read-more">Read more</a>
                            </div>
                        </div>

                        <div class="section-post">
                            <div class="section-post-media">
                                <a href="#" title="San Francisco Felt Pennant">
                                    <img src="https://i2-prod.mirror.co.uk/incoming/article10396011.ece/ALTERNATES/s615/PROD-Surgeons-performing-surgery-in-operating-room.jpg" alt="">
                                </a>
                            </div>

                            <div class="section-post-body">
                                <h4 class="section-post-title">
                                    <a href="#" title="San Francisco Felt Pennant">Lorem ipsum dolor sit</a>
                                </h4>

                                <p class="section-post-excerpt">Lorem Ipsum is simply dummy text of the hospitals and operations industry.</p>

                                <a href="#" title="" class="read-more">Read more</a>
                            </div>
                        </div>

                        <div class="section-post">
                            <div class="section-post-media">
                                <a href="#" title="San Francisco Felt Pennant">
                                    <img src="https://www.happy-or-not.com/content/uploads/2017/03/Pharmacy.jpg" alt="">
                                </a>
                            </div>

                            <div class="section-post-body">

                                <h4 class="section-post-title">
                                    <a href="#" title="San Francisco Felt Pennant">San Francisco Felt Pennant</a>
                                </h4>

                                <p class="section-post-excerpt">Lorem Ipsum is simply dummy text of the printing and typeseting industry.</p>

                                <a href="#" title="" class="read-more">Read more</a>
                            </div>
                        </div>

                        <div class="section-post">
                            <div class="section-post-media">
                                <a href="#" title="San Francisco Felt Pennant">
                                    <img src="https://www.happy-or-not.com/content/uploads/2017/03/Pharmacy.jpg" alt="">
                                </a>
                            </div>

                            <div class="section-post-body">
                                <div class="section-post-time">
                                    <span>18</span>  <small>August</small>
                                </div>

                                <h4 class="section-post-title">
                                    <a href="#" title="San Francisco Felt Pennant">San Francisco Felt Pennant</a>
                                </h4>

                                <p class="section-post-excerpt">Lorem Ipsum is simply dummy text of the printing and typeseting industry.</p>

                                <a href="#" title="" class="read-more">Read more</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section>
        <div class="container">

            <div class="margin-bottom-50">
                <div class="subscible-wrapper subscible-inline">

                    <div class="row">
                        <div class="col-md-2">
                            <h3 class="subscribe-title">Subscribe newsletter</h3>
                        </div>

                        <div class="col-md-4">
                            <div class="subscribe-comment">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <form method="GET" action="index.html" class="subscible-form">
                                <div class="form-group">
                                    <label class="sr-only" for="subscribe-email">Email</label>
                                    <input type="email" placeholder="Enter your email address" class="form-control" id="subscribe-email">
                                </div>

                                <div class="form-submit">
                                    <button class="btn btn-lg btn-primary" type="submit">Subscribe email</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.subscible-wrapper -->
            </div>

        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section>
        <div class="section-brands">
            <div class="container">
                <div class="brands-carousel owl-carousel" id="brands-carousel">


                    <div class="center">
                        <img src="{{ URL::To('/') }}/assets/images/clients/1.png" alt="">
                    </div>
                    <!-- /.center -->

                    <div class="center">
                        <img src="{{ URL::To('/') }}/assets/images/clients/2.png" alt="">
                    </div>
                    <!-- /.center -->

                    <div class="center">
                        <img src="{{ URL::To('/') }}/assets/images/clients/3.png" alt="">
                    </div>
                    <!-- /.center -->

                    <div class="center">
                        <img src="{{ URL::To('/') }}/assets/images/clients/4.png" alt="">
                    </div>
                    <!-- /.center -->



                    <div class="center">
                        <img src="{{ URL::To('/') }}/assets/images/clients/5.png" alt="">
                    </div>
                    <!-- /.center -->

                    <div class="center">
                        <img src="{{ URL::To('/') }}/assets/images/clients/6.png" alt="">
                    </div>
                    <!-- /.center -->

                    <div class="center">
                        <img src="{{ URL::To('/') }}/assets/images/clients/7.png" alt="">
                    </div>
                    <!-- /.center -->

                    <div class="center">
                        <img src="{{ URL::To('/') }}/assets/images/clients/1.png" alt="">
                    </div>
                    <!-- /.center -->


                </div>
                <!-- /.brands-carousel -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.section-brands -->
    </section>
    <!-- /section -->

@endsection

@section('scripts')
    {{ HTML::script('assets/js/default.js') }}
@endsection
