<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/03/18 10:29.
 */
?>
<div class="product-quickview-popup-container pre-loading">
    <div class="white-popup product-quickview-popup">
        <div class="product">
            <i class="fa fa-close" onclick="closeProductQuickView();"></i>
            <div class="product-media">
                <div id="quck-view-imgs" class="product-quickview-slider owl-carousel owl-carousel-inset">

                </div>
            </div>
            <!-- /.product-media -->

            <div class="product-body">
                <h2 class="product-name">
                    <a id="quick-view-product-name" href="##"> </a>
                </h2>
                <!-- /.product-name -->

                <div class="product-status">
                    <span>In Stock</span>
                    <span>-</span>
                    <span id="quick-view-product-id"></span>
                </div>
                <!-- /.product-status -->

                <div class="product-price">
                    <span id="quick-view-product-price" class="amount"></span>
                </div>
                <!-- /.product-price -->

                <div class="product-description">
                    <p id="quick-view-product-small-description"></p>
                </div>

                <div class="product-list-actions-wrapper">
                    <form action="#" method="POST">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="p_color">Color</label>
                                    <select name="p_color" id="p_color" class="form-control">
                                        <option value="">Blue</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="p_size">Size</label>
                                    <select name="p_size" id="p_size" class="form-control">
                                        <option value="">100ml</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="p_qty">Qty</label>
                                    <select name="p_qty" id="p_qty" class="form-control">
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- /.form -->

                    <div class="product-list-actions">
                        <button class="btn btn-lg btn-primary">Add to cart</button>
                        <button class="btn btn-lg btn-dark btn-outline">Add to wishlist</button>
                    </div>
                    <!-- /.product-actions -->
                </div>

                <div class="product-meta">
                <span class="product-category">
                    <span>Category:</span>
                    <a id="quick-view-product-category" href="##" title=""></a>
                </span>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                <span class="product-tags">
                    <span>Brand:</span>
                    <a id="quck-view-product-manufacture" href="##" title="">Tooth Care</a>
                </span>
                </div>

            </div>
            <!-- /.product-body -->
        </div>
    </div>
</div>
