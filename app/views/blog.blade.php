<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 19/03/18 13:06.
 */
?>

@extends('master')

@section('content')

    <div class="main-header background" style="background-image: url('{{URL::To('/')}}/assets/images/backgrounds/blog.jpg');">
        <div class="container">
            <h1 style="color: #00b4af; text-shadow: 1px 1px 2px rgba(0,0,0,0.6);">Blog Masonry</h1>
        </div>
    </div>


    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="blog-masonry.html#">Home</a>
                </li>
                <li class="active"><span>Blog Masonry</span>
                </li>
            </ol>

        </div>
    </div>


    <div class="blog-wrapper blog-masonry">
        <div class="container">
            <div class="row row-masonry">

                <div class="column col-md-4 col-sm-6 col-xs-12">
                    <article class="post">
                        <div class="entry-media">
                            <div class="entry-slide">
                                <div class="entry-carousel owl-carousel">
                                    <div>
                                        <img src="{{ URL::To('/') }}/html/images/posts/sidebar/1.jpg" alt="">
                                    </div>
                                    <div>
                                        <img src="{{ URL::To('/') }}/html/images/posts/sidebar/1.jpg" alt="">
                                    </div>
                                    <div>
                                        <img src="{{ URL::To('/') }}/html/images/posts/sidebar/1.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <!-- /.entry-slider -->
                        </div>
                        <!-- /.entry-media -->

                        <div class="entry-summary">
                            <div class="entry-datetime">
                                <a href="blog-masonry.html#" title="">
                                    <span class="entry-day">28</span>
                                    <span class="entry-month">/June</span>
                                </a>
                            </div>
                            <!-- /.entry-datetime -->

                            <div class="entry-title">
                                <h2><a href="blog-masonry.html#" title="">Help The Body Run More Beautiful When Wearing</a></h2>
                            </div>
                            <!-- /.entry-title -->

                            <div class="entry-excerpt">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                                    it to make a type specimen book. It has survived not only five centuries...</p>
                            </div>
                            <!-- /.entry-excerpt -->

                            <div class="entry-meta">
                                <span>Posted by</span>
                                <strong class="entry-author"><a href="blog-masonry.html#" title="">Join Doe </a></strong>
                                <span>-</span>
                                <span class="entry-category"><a href="blog-masonry.html#" title="">Clothing</a>  </span>
                                <span>-</span>
                                <span class="entry-comment-count"><a href="blog-masonry.html#" title="">3 Comments</a></span>
                            </div>
                            <!-- /.entry-meta -->
                        </div>
                        <!-- /.entry-summary -->
                    </article>
                    <!-- /.post -->
                </div>

                <div class="column col-md-4 col-sm-6">
                    <article class="post">
                        <div class="entry-media">
                            <div class="entry-thumbnail">
                                <img src="{{ URL::To('/') }}/html/images/posts/masonry/1.jpg" alt="">
                            </div>
                            <!-- /.entry-post-thumbnail -->

                            <div class="entry-format">
                                <span class="play-button" aria-label="Play"></span>
                            </div>
                        </div>
                        <!-- /.entry-media -->

                        <div class="entry-summary">
                            <div class="entry-datetime">
                                <a href="blog-masonry.html#" title="">
                                    <span class="entry-day">28</span>
                                    <span class="entry-month">/June</span>
                                </a>
                            </div>
                            <!-- /.entry-datetime -->

                            <div class="entry-title">
                                <h2><a href="blog-masonry.html#" title="">Help The Body Run More Beautiful When Wearing</a></h2>
                            </div>
                            <!-- /.entry-title -->

                            <div class="entry-excerpt">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                                    it to make a type specimen book. It has survived not only five centuries...</p>
                            </div>
                            <!-- /.entry-excerpt -->

                            <div class="entry-meta">
                                <span>Posted by</span>
                                <strong class="entry-author"><a href="blog-masonry.html#" title="">Join Doe </a></strong>
                                <span>-</span>
                                <span class="entry-category"><a href="blog-masonry.html#" title="">Clothing</a>  </span>
                                <span>-</span>
                                <span class="entry-comment-count"><a href="blog-masonry.html#" title="">3 Comments</a></span>
                            </div>
                            <!-- /.entry-meta -->
                        </div>
                        <!-- /.entry-summary -->
                    </article>
                    <!-- /.post -->
                </div>


                <div class="column col-md-4 col-sm-6">

                    <article class="post">
                        <div class="entry-media">
                            <div class="entry-feature-image">
                                <img src="{{ URL::To('/') }}/html/images/posts/masonry/3.jpg" alt="">
                            </div>
                            <!-- /.entry-post-thumbnail -->
                        </div>
                        <!-- /.entry-media -->

                        <div class="entry-summary">
                            <div class="entry-datetime">
                                <a href="blog-masonry.html#" title="">
                                    <span class="entry-day">28</span>
                                    <span class="entry-month">/June</span>
                                </a>
                            </div>
                            <!-- /.entry-datetime -->

                            <div class="entry-title">
                                <h2><a href="blog-masonry.html#" title="">Help The Body Run More Beautiful When Wearing</a></h2>
                            </div>
                            <!-- /.entry-title -->

                            <div class="entry-excerpt">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                                    it to make a type specimen book. It has survived not only five centuries...</p>
                            </div>
                            <!-- /.entry-excerpt -->

                            <div class="entry-meta">
                                <span>Posted by</span>
                                <strong class="entry-author"><a href="blog-masonry.html#" title="">Join Doe </a></strong>
                                <span>-</span>
                                <span class="entry-category"><a href="blog-masonry.html#" title="">Clothing</a>  </span>
                                <span>-</span>
                                <span class="entry-comment-count"><a href="blog-masonry.html#" title="">3 Comments</a></span>
                            </div>
                            <!-- /.entry-meta -->
                        </div>
                        <!-- /.entry-summary -->
                    </article>
                    <!-- /.post -->

                </div>

                <div class="column col-md-4 col-sm-6">

                    <article class="post">
                        <div class="entry-media">
                            <div class="entry-feature-image">
                                <img src="{{ URL::To('/') }}/html/images/posts/masonry/4.jpg" alt="">
                            </div>
                            <!-- /.entry-post-thumbnail -->
                        </div>
                        <!-- /.entry-media -->

                        <div class="entry-summary">
                            <div class="entry-datetime">
                                <a href="blog-masonry.html#" title="">
                                    <span class="entry-day">28</span>
                                    <span class="entry-month">/June</span>
                                </a>
                            </div>
                            <!-- /.entry-datetime -->

                            <div class="entry-title">
                                <h2><a href="blog-masonry.html#" title="">Help The Body Run More Beautiful When Wearing</a></h2>
                            </div>
                            <!-- /.entry-title -->

                            <div class="entry-excerpt">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                                    it to make a type specimen book. It has survived not only five centuries...</p>
                            </div>
                            <!-- /.entry-excerpt -->

                            <div class="entry-meta">
                                <span>Posted by</span>
                                <strong class="entry-author"><a href="blog-masonry.html#" title="">Join Doe </a></strong>
                                <span>-</span>
                                <span class="entry-category"><a href="blog-masonry.html#" title="">Clothing</a>  </span>
                                <span>-</span>
                                <span class="entry-comment-count"><a href="blog-masonry.html#" title="">3 Comments</a></span>
                            </div>
                            <!-- /.entry-meta -->
                        </div>
                        <!-- /.entry-summary -->
                    </article>
                    <!-- /.post -->

                </div>

                <div class="column col-md-4 col-sm-6">

                    <article class="post">
                        <div class="entry-media">
                            <div class="entry-feature-image">
                                <img src="{{ URL::To('/') }}/html/images/posts/masonry/5.jpg" alt="">
                            </div>
                            <!-- /.entry-post-thumbnail -->
                        </div>
                        <!-- /.entry-media -->

                        <div class="entry-summary">
                            <div class="entry-datetime">
                                <a href="blog-masonry.html#" title="">
                                    <span class="entry-day">28</span>
                                    <span class="entry-month">/June</span>
                                </a>
                            </div>
                            <!-- /.entry-datetime -->

                            <div class="entry-title">
                                <h2><a href="blog-masonry.html#" title="">Help The Body Run More Beautiful When Wearing</a></h2>
                            </div>
                            <!-- /.entry-title -->

                            <div class="entry-excerpt">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                                    it to make a type specimen book. It has survived not only five centuries...</p>
                            </div>
                            <!-- /.entry-excerpt -->

                            <div class="entry-meta">
                                <span>Posted by</span>
                                <strong class="entry-author"><a href="blog-masonry.html#" title="">Join Doe </a></strong>
                                <span>-</span>
                                <span class="entry-category"><a href="blog-masonry.html#" title="">Clothing</a>  </span>
                                <span>-</span>
                                <span class="entry-comment-count"><a href="blog-masonry.html#" title="">3 Comments</a></span>
                            </div>
                            <!-- /.entry-meta -->
                        </div>
                        <!-- /.entry-summary -->
                    </article>
                    <!-- /.post -->

                </div>

                <div class="column col-md-4 col-sm-6">

                    <article class="post">
                        <div class="entry-media">
                            <div class="entry-feature-image">
                                <img src="{{ URL::To('/') }}/html/images/posts/masonry/6.jpg" alt="">
                            </div>
                            <!-- /.entry-post-thumbnail -->
                        </div>
                        <!-- /.entry-media -->

                        <div class="entry-summary">
                            <div class="entry-datetime">
                                <a href="blog-masonry.html#" title="">
                                    <span class="entry-day">28</span>
                                    <span class="entry-month">/June</span>
                                </a>
                            </div>
                            <!-- /.entry-datetime -->

                            <div class="entry-title">
                                <h2><a href="blog-masonry.html#" title="">Help The Body Run More Beautiful When Wearing</a></h2>
                            </div>
                            <!-- /.entry-title -->

                            <div class="entry-excerpt">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                                    it to make a type specimen book. It has survived not only five centuries...</p>
                            </div>
                            <!-- /.entry-excerpt -->

                            <div class="entry-meta">
                                <span>Posted by</span>
                                <strong class="entry-author"><a href="blog-masonry.html#" title="">Join Doe </a></strong>
                                <span>-</span>
                                <span class="entry-category"><a href="blog-masonry.html#" title="">Clothing</a>  </span>
                                <span>-</span>
                                <span class="entry-comment-count"><a href="blog-masonry.html#" title="">3 Comments</a></span>
                            </div>
                            <!-- /.entry-meta -->
                        </div>
                        <!-- /.entry-summary -->
                    </article>
                    <!-- /.post -->

                </div>

                <div class="column col-md-4 col-sm-6">

                    <article class="post">
                        <div class="entry-media">
                            <div class="entry-feature-image">
                                <img src="{{ URL::To('/') }}/html/images/posts/masonry/7.jpg" alt="">
                            </div>
                            <!-- /.entry-post-thumbnail -->
                        </div>
                        <!-- /.entry-media -->

                        <div class="entry-summary">
                            <div class="entry-datetime">
                                <a href="blog-masonry.html#" title="">
                                    <span class="entry-day">28</span>
                                    <span class="entry-month">/June</span>
                                </a>
                            </div>
                            <!-- /.entry-datetime -->

                            <div class="entry-title">
                                <h2><a href="blog-masonry.html#" title="">Help The Body Run More Beautiful When Wearing</a></h2>
                            </div>
                            <!-- /.entry-title -->

                            <div class="entry-excerpt">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                                    it to make a type specimen book. It has survived not only five centuries...</p>
                            </div>
                            <!-- /.entry-excerpt -->

                            <div class="entry-meta">
                                <span>Posted by</span>
                                <strong class="entry-author"><a href="blog-masonry.html#" title="">Join Doe </a></strong>
                                <span>-</span>
                                <span class="entry-category"><a href="blog-masonry.html#" title="">Clothing</a>  </span>
                                <span>-</span>
                                <span class="entry-comment-count"><a href="blog-masonry.html#" title="">3 Comments</a></span>
                            </div>
                            <!-- /.entry-meta -->
                        </div>
                        <!-- /.entry-summary -->
                    </article>
                    <!-- /.post -->

                </div>

            </div>
            <!-- /.row -->

            <div class="center">
                <button class="btn btn-lg btn-dark btn-outline">Load more</button>
            </div>
            <!-- /.center -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /.blog-wrapper -->

    <script>
        $(function() { aweBlogMasonry(); });
    </script>
@endsection