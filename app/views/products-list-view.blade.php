<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/03/18 10:54.
 */
?>

@extends('master')

@section('content')

    <div class="main-header background background-image-heading-products" style="background: url('{{ URL::To('/') }}/assets/images/backgrounds/heading-products.jpg');">
        <div class="container">
            <h1 style="color: #00b4af;">Baby & Child Care</h1>
        </div>
    </div>


    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ URL::Route('home') }}">Home</a>
                </li>
                <li class="active"><span>Baby & Child Care</span>
                </li>
            </ol>

        </div>
    </div>


    <div class="container">
        <div class="row">

            <div class="col-md-9 col-md-push-3">

                <div class="product-header-actions">
                    <form action="{{ URL::To('/') }}" method="POST" class="form-inline">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="view-icons">
                                    <a href="#" class="view-icon "><span class="icon icon-th"></span></a>
                                    <a href="#" class="view-icon active"><span class="icon icon-th-list"></span></a>
                                </div>

                                <div class="view-count">
                                    <span class="text-muted">Item 1 to 9 of 30 Items</span>
                                </div>
                            </div>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <div class="form-show-sort">
                                    <div class="form-group pull-left">
                                        <label for="p_show">Show</label>
                                        <select name="p_show" id="p_show" class="form-control input-sm">
                                            <option value="">10</option>
                                            <option value="">25</option>
                                            <option value="">50</option>
                                        </select>
                                        <strong>per page</strong>
                                    </div>
                                    <!-- /.form-group -->

                                    <div class="form-group pull-right text-right">
                                        <label for="p_sort_by">Sort By</label>
                                        <select name="p_sort_by" id="p_sort_by" class="form-control input-sm">
                                            <option value="">Lastest</option>
                                            <option value="">Recommend</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </form>
                </div>
                <!-- /.product-header-actions -->


                <div class="products">

                    <div class="col-md-12">

                        <div class="product product-list">
                            <div class="product-media">
                                <div class="product-thumbnail">
                                    <a href="#" title="">
                                        <img src="{{ URL::To('/') }}/assets/images/products/1.jpg" alt="">
                                    </a>
                                </div>
                                <!-- /.product-thumbnail -->


                                <div class="product-hover">
                                    <div class="product-actions">
                                        <button class="">
                                            <span class="icon icon-shopping-bag"></span>
                                        </button>

                                        <button class="">
                                            <span class="icon icon-star"></span>
                                        </button>

                                        <button class="">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.product-hover -->



                                <span class="product-label hot">
                                    <span>Famous</span>
                                        </span>

                            </div>
                            <!-- /.product-media -->

                            <div class="product-body">
                                <h2 class="product-name">
                                    <a href="#" title="Gin Lane Greenport Cotton Shirt">Lorem Ispeum Consectetuer</a>
                                </h2>
                                <!-- /.product-product -->

                                <div class="product-category">
                                    <span>Oral Care</span>
                                </div>
                                <!-- /.product-category -->

                                <div class="product-price">

                                    <span class="amount">$24</span>

                                </div>
                                <!-- /.product-price -->

                                <div class="product-stars">
                                            <span class="rating">
                                        <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            </span>
                                </div>

                                <div class="product-description">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                </div>

                                <div class="product-list-actions">
                                    <button class="btn btn-lg btn-primary">Add to cart</button>
                                    <button class="btn btn-lg btn-dark btn-outline">Add to wishlist</button>
                                </div>
                                <!-- /.product-actions -->

                            </div>
                            <!-- /.product-body -->
                        </div>
                        <!-- /.product -->

                    </div>

                    <div class="col-md-12">

                        <div class="product product-list">
                            <div class="product-media">
                                <div class="product-thumbnail">
                                    <a href="#" title="">
                                        <img src="{{ URL::To('/') }}/assets/images/products/2.jpg" alt="">
                                    </a>
                                </div>
                                <!-- /.product-thumbnail -->


                                <div class="product-hover">
                                    <div class="product-actions">
                                        <button class="">
                                            <span class="icon icon-shopping-bag"></span>
                                        </button>

                                        <button class="">
                                            <span class="icon icon-star"></span>
                                        </button>

                                        <button class="">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.product-hover -->



                            </div>
                            <!-- /.product-media -->

                            <div class="product-body">
                                <h2 class="product-name">
                                    <a href="#" title="Gin Lane Greenport Cotton Shirt">Lorem Ispeum Consectetuer</a>
                                </h2>
                                <!-- /.product-product -->

                                <div class="product-category">
                                    <span>Oral Care</span>
                                </div>
                                <!-- /.product-category -->

                                <div class="product-price">

                                    <span class="amount">$6</span>

                                </div>
                                <!-- /.product-price -->

                                <div class="product-stars">
                                            <span class="rating">
                                        <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            </span>
                                </div>

                                <div class="product-description">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                </div>

                                <div class="product-list-actions">
                                    <button class="btn btn-lg btn-primary">Add to cart</button>
                                    <button class="btn btn-lg btn-dark btn-outline">Add to wishlist</button>
                                </div>
                                <!-- /.product-actions -->

                            </div>
                            <!-- /.product-body -->
                        </div>
                        <!-- /.product -->

                    </div>

                    <div class="col-md-12">

                        <div class="product product-list">
                            <div class="product-media">
                                <div class="product-thumbnail">
                                    <a href="#" title="">
                                        <img src="{{ URL::To('/') }}/assets/images/products/3.jpg" alt="">
                                    </a>
                                </div>
                                <!-- /.product-thumbnail -->


                                <div class="product-hover">
                                    <div class="product-actions">
                                        <button class="">
                                            <span class="icon icon-shopping-bag"></span>
                                        </button>

                                        <button class="">
                                            <span class="icon icon-star"></span>
                                        </button>

                                        <button class="">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.product-hover -->



                                <span class="product-label sale">
                                    <span>sale</span>
                                        </span>

                            </div>
                            <!-- /.product-media -->

                            <div class="product-body">
                                <h2 class="product-name">
                                    <a href="#" title="Gin Lane Greenport Cotton Shirt">Lorem Ispeum Consectetuer</a>
                                </h2>
                                <!-- /.product-product -->

                                <div class="product-category">
                                    <span>Oral Care</span>
                                </div>
                                <!-- /.product-category -->

                                <div class="product-price">

                                    <span class="amount">$12</span>
                                    <del class="amount">$14</del>

                                </div>
                                <!-- /.product-price -->

                                <div class="product-stars">
                                            <span class="rating">
                                        <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="fa fa-star-half"></span>
                                            </span>
                                </div>

                                <div class="product-description">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                </div>

                                <div class="product-list-actions">
                                    <button class="btn btn-lg btn-primary">Add to cart</button>
                                    <button class="btn btn-lg btn-dark btn-outline">Add to wishlist</button>
                                </div>
                                <!-- /.product-actions -->

                            </div>
                            <!-- /.product-body -->
                        </div>
                        <!-- /.product -->

                    </div>

                    <div class="col-md-12">

                        <div class="product product-list">
                            <div class="product-media">
                                <div class="product-thumbnail">
                                    <a href="#" title="">
                                        <img src="{{ URL::To('/') }}/assets/images/products/4.jpg" alt="">
                                    </a>
                                </div>
                                <!-- /.product-thumbnail -->


                                <div class="product-hover">
                                    <div class="product-actions">
                                        <button class="">
                                            <span class="icon icon-shopping-bag"></span>
                                        </button>

                                        <button class="">
                                            <span class="icon icon-star"></span>
                                        </button>

                                        <button class="">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.product-hover -->


                            </div>
                            <!-- /.product-media -->

                            <div class="product-body">
                                <h2 class="product-name">
                                    <a href="#" title="Gin Lane Greenport Cotton Shirt">Lorem Ispeum Consectetuer</a>
                                </h2>
                                <!-- /.product-product -->

                                <div class="product-category">
                                    <span>Oral Care</span>
                                </div>
                                <!-- /.product-category -->

                                <div class="product-price">

                                    <span class="amount">$12</span>

                                </div>
                                <!-- /.product-price -->

                                <div class="product-stars">
                                            <span class="rating">
                                        <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="fa fa-star-half"></span>
                                            <span class="fa fa-star-half"></span>
                                            </span>
                                </div>

                                <div class="product-description">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                </div>

                                <div class="product-list-actions">
                                    <button class="btn btn-lg btn-primary">Add to cart</button>
                                    <button class="btn btn-lg btn-dark btn-outline">Add to wishlist</button>
                                </div>
                                <!-- /.product-actions -->

                            </div>
                            <!-- /.product-body -->
                        </div>
                        <!-- /.product -->

                    </div>

                    <div class="col-md-12">

                        <div class="product product-list">
                            <div class="product-media">
                                <div class="product-thumbnail">
                                    <a href="#" title="">
                                        <img src="{{ URL::To('/') }}/assets/images/products/5.jpg" alt="">
                                    </a>
                                </div>
                                <!-- /.product-thumbnail -->


                                <div class="product-hover">
                                    <div class="product-actions">
                                        <button class="">
                                            <span class="icon icon-shopping-bag"></span>
                                        </button>

                                        <button class="">
                                            <span class="icon icon-star"></span>
                                        </button>

                                        <button class="">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.product-hover -->



                                <span class="product-label new">
                                    <span>new</span>
                                        </span>

                            </div>
                            <!-- /.product-media -->

                            <div class="product-body">
                                <h2 class="product-name">
                                    <a href="#" title="Gin Lane Greenport Cotton Shirt">Lorem Ispeum Consectetuer</a>
                                </h2>
                                <!-- /.product-product -->

                                <div class="product-category">
                                    <span>Oral Care</span>
                                </div>
                                <!-- /.product-category -->

                                <div class="product-price">

                                    <span class="amount">$14</span>

                                </div>
                                <!-- /.product-price -->

                                <div class="product-stars">
                                            <span class="rating">
                                        <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            </span>
                                </div>

                                <div class="product-description">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                </div>

                                <div class="product-list-actions">
                                    <button class="btn btn-lg btn-primary">Add to cart</button>
                                    <button class="btn btn-lg btn-dark btn-outline">Add to wishlist</button>
                                </div>
                                <!-- /.product-actions -->

                            </div>
                            <!-- /.product-body -->
                        </div>
                        <!-- /.product -->

                    </div>

                    <div class="col-md-12">

                        <div class="product product-list">
                            <div class="product-media">
                                <div class="product-thumbnail">
                                    <a href="#" title="">
                                        <img src="{{ URL::To('/') }}/assets/images/products/1.jpg" alt="">
                                    </a>
                                </div>
                                <!-- /.product-thumbnail -->


                                <div class="product-hover">
                                    <div class="product-actions">
                                        <button class="">
                                            <span class="icon icon-shopping-bag"></span>
                                        </button>

                                        <button class="">
                                            <span class="icon icon-star"></span>
                                        </button>

                                        <button class="">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.product-hover -->



                            </div>
                            <!-- /.product-media -->

                            <div class="product-body">
                                <h2 class="product-name">
                                    <a href="#" title="Gin Lane Greenport Cotton Shirt">Lorem Ispeum Consectetuer</a>
                                </h2>
                                <!-- /.product-product -->

                                <div class="product-category">
                                    <span>Oral Care</span>
                                </div>
                                <!-- /.product-category -->

                                <div class="product-price">

                                    <span class="amount">$5</span>

                                </div>
                                <!-- /.product-price -->

                                <div class="product-stars">
                                            <span class="rating">
                                        <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            </span>
                                </div>

                                <div class="product-description">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                </div>

                                <div class="product-list-actions">
                                    <button class="btn btn-lg btn-primary">Add to cart</button>
                                    <button class="btn btn-lg btn-dark btn-outline">Add to wishlist</button>
                                </div>
                                <!-- /.product-actions -->

                            </div>
                            <!-- /.product-body -->
                        </div>
                        <!-- /.product -->

                    </div>

                    <div class="col-md-12">

                        <div class="product product-list">
                            <div class="product-media">
                                <div class="product-thumbnail">
                                    <a href="#" title="">
                                        <img src="{{ URL::To('/') }}/assets/images/products/2.jpg" alt="">
                                    </a>
                                </div>
                                <!-- /.product-thumbnail -->


                                <div class="product-hover">
                                    <div class="product-actions">
                                        <button class="">
                                            <span class="icon icon-shopping-bag"></span>
                                        </button>

                                        <button class="">
                                            <span class="icon icon-star"></span>
                                        </button>

                                        <button class="">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.product-hover -->



                            </div>
                            <!-- /.product-media -->

                            <div class="product-body">
                                <h2 class="product-name">
                                    <a href="#" title="Gin Lane Greenport Cotton Shirt">Lorem Ispeum Consectetuer</a>
                                </h2>
                                <!-- /.product-product -->

                                <div class="product-category">
                                    <span>Oral Care</span>
                                </div>
                                <!-- /.product-category -->

                                <div class="product-price">

                                    <span class="amount">$12</span>

                                </div>
                                <!-- /.product-price -->

                                <div class="product-stars">
                                            <span class="rating">
                                        <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            </span>
                                </div>

                                <div class="product-description">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                </div>

                                <div class="product-list-actions">
                                    <button class="btn btn-lg btn-primary">Add to cart</button>
                                    <button class="btn btn-lg btn-dark btn-outline">Add to wishlist</button>
                                </div>
                                <!-- /.product-actions -->

                            </div>
                            <!-- /.product-body -->
                        </div>
                        <!-- /.product -->

                    </div>

                    <div class="col-md-12">

                        <div class="product product-list">
                            <div class="product-media">
                                <div class="product-thumbnail">
                                    <a href="#" title="">
                                        <img src="{{ URL::To('/') }}/assets/images/products/4.jpg" alt="">
                                    </a>
                                </div>
                                <!-- /.product-thumbnail -->


                                <div class="product-hover">
                                    <div class="product-actions">
                                        <button class="">
                                            <span class="icon icon-shopping-bag"></span>
                                        </button>

                                        <button class="">
                                            <span class="icon icon-star"></span>
                                        </button>

                                        <button class="">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.product-hover -->



                            </div>
                            <!-- /.product-media -->

                            <div class="product-body">
                                <h2 class="product-name">
                                    <a href="#" title="Gin Lane Greenport Cotton Shirt">Lorem Ispeum Consectetuer</a>
                                </h2>
                                <!-- /.product-product -->

                                <div class="product-category">
                                    <span>Oral Care</span>
                                </div>
                                <!-- /.product-category -->

                                <div class="product-price">

                                    <span class="amount">$36</span>

                                </div>
                                <!-- /.product-price -->

                                <div class="product-stars">
                                            <span class="rating">
                                        <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            </span>
                                </div>

                                <div class="product-description">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                </div>

                                <div class="product-list-actions">
                                    <button class="btn btn-lg btn-primary">Add to cart</button>
                                    <button class="btn btn-lg btn-dark btn-outline">Add to wishlist</button>
                                </div>
                                <!-- /.product-actions -->

                            </div>
                            <!-- /.product-body -->
                        </div>
                        <!-- /.product -->

                    </div>

                    <div class="col-md-12">

                        <div class="product product-list">
                            <div class="product-media">
                                <div class="product-thumbnail">
                                    <a href="#" title="">
                                        <img src="{{ URL::To('/') }}/assets/images/products/5.jpg" alt="">
                                    </a>
                                </div>
                                <!-- /.product-thumbnail -->


                                <div class="product-hover">
                                    <div class="product-actions">
                                        <button class="">
                                            <span class="icon icon-shopping-bag"></span>
                                        </button>

                                        <button class="">
                                            <span class="icon icon-star"></span>
                                        </button>

                                        <button class="">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.product-hover -->



                            </div>
                            <!-- /.product-media -->

                            <div class="product-body">
                                <h2 class="product-name">
                                    <a href="#" title="Gin Lane Greenport Cotton Shirt">Lorem Ispeum Consectetuer</a>
                                </h2>
                                <!-- /.product-product -->

                                <div class="product-category">
                                    <span>Oral Care</span>
                                </div>
                                <!-- /.product-category -->

                                <div class="product-price">

                                    <span class="amount">$12</span>

                                </div>
                                <!-- /.product-price -->

                                <div class="product-stars">
                                            <span class="rating">
                                        <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            </span>
                                </div>

                                <div class="product-description">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                </div>

                                <div class="product-list-actions">
                                    <button class="btn btn-lg btn-primary">Add to cart</button>
                                    <button class="btn btn-lg btn-dark btn-outline">Add to wishlist</button>
                                </div>
                                <!-- /.product-actions -->

                            </div>
                            <!-- /.product-body -->
                        </div>
                        <!-- /.product -->

                    </div>

                </div>
                <!-- /.products -->


                <ul class="pagination">
                    <li class="pagination-prev"><a href="#"><i class="icon icon-arrow-prev"></i></a>
                    </li>
                    <li><a href="#">1</a>
                    </li>
                    <li class="active"><a href="#">2</a>
                    </li>
                    <li><a href="#">3</a>
                    </li>
                    <li><a href="#">4</a>
                    </li>
                    <li><a href="#">5</a>
                    </li>
                    <li><span>...</span>
                    </li>
                    <li><a href="#">15</a>
                    </li>
                    <li class="pagination-next"><a href="#"><i class="icon icon-arrow-next"></i></a>
                    </li>
                </ul>
                <!-- ./pagination -->

            </div>
            <!-- /.col-* -->

            <div class="col-md-3 col-md-pull-9">
                <div id="shop-widgets-filters" class="shop-widgets-filters">

                    <div id="widget-area" class="widget-area">

                        <div class="widget woocommerce widget_product_categories">
                            <h3 class="widget-title">Categories</h3>

                            <ul>
                                <li class="active"><a href="products-grid.html#" title="" style="font-weight: 600;">All Clothing</a>
                                </li>
                                <li><a href="products-grid.html#" title="">For Baby & Child &nbsp;<i class="fa fa-angle-down"></i> </a>
                                    <ul style="margin-left: 25px; font-size: 12px;">
                                        <li><a href="#">Baby Bath</a></li>
                                        <li><a href="#">Baby Lotion & Oils </a></li>
                                        <li><a href="#">Baby Powder </a></li>
                                        <li><a href="#">Baby Nappies </a></li>
                                        <li><a href="#">Baby Wipes </a></li>
                                        <li><a href="#">Nutrition Supplements </a></li>
                                    </ul>
                                </li>
                                <li><a href="products-grid.html#" title="">For Mom &nbsp;<i class="fa fa-angle-up"></i> </a>
                                </li>
                                <li><a href="products-grid.html#" title="">Accessories &nbsp;<i class="fa fa-angle-up"></i> </a>
                                </li>

                            </ul>
                        </div>
                        <!-- /.widget -->




                        <div class="widget woocommerce">
                            <h3 class="widget-title">Volume</h3>

                            <div class="widget-content">
                                <label class="label-select">
                                    <select name="product-sizes" class="form-control">
                                        <option value="">0ml - 100ml</option>
                                        <option value="">Size B</option>
                                        <option value="">Size C</option>
                                        <option value="">Size D</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <!-- /.widget -->


                        <div class="widget">
                            <h3 class="widget-title">Brands</h3>

                            <div class="widget-content">
                                <div class="awewoo-brand">
                                    <div class="awewoo-brand-header">
                                        <input type="text" class="form-control" placeholder="Find your brand">
                                    </div>

                                    <div class="awewoo-brand-content">
                                        <div class="nano" style="max-height: 150px;">
                                            <div class="nano-content">
                                                <ul>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Synxsole</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Bobble</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Burnaid</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Double "D"</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Moxie</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>RID</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Gluco</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Hood</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Kill City</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Police</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Vans</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Hood</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Kill City</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Police</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Vans</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Hood</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Kill City</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>Baby Milo</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span>The Police</span>
                                                            </label>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="widget woocommerce widget_product_prices_filter">
                            <h3 class="widget-title">Prices</h3>

                            <div class="widget-content">
                                <div class="ranger-wrapper">
                                    <div id="price-slider" class="ranger"></div>
                                </div>

                                <div class="center small gray">
                                    <span>Start from</span>
                                    <span id="amount" class="dark bold">$35</span>
                                    <span>to</span>
                                    <span class="dark bold">$320</span>
                                </div>
                            </div>
                        </div>

                        <script>
                            $(function() { awePriceSlider(); });
                        </script>


                        <div class="widget">
                            <h3 class="widget-title">Colors</h3>

                            <div class="wiget-content">
                                <div class="colors square">
                                    <a href="products-grid.html#" title=""><span class="color orange"></span></a>
                                    <a href="products-grid.html#" title=""><span class="color green"></span></a>
                                    <a href="products-grid.html#" title=""><span class="color blue"></span></a>
                                    <a href="products-grid.html#" title=""><span class="color dark"></span></a>
                                    <a href="products-grid.html#" title=""><span class="color gray"></span></a>
                                    <a href="products-grid.html#" title=""><span class="color white"></span></a>
                                </div>
                            </div>
                        </div>


                        <div class="widget woocommerce widget_product_prices">
                            <h3 class="widget-title">Prices</h3>

                            <ul>
                                <li><a href="products-grid.html#" title="">None</a>
                                </li>
                                <li><a href="products-grid.html#" title="">$35  -  $100</a>
                                </li>
                                <li class="active"><a href="products-grid.html#" title="">$100 - $200</a>
                                </li>
                                <li><a href="products-grid.html#" title="">$200 - $300</a>
                                </li>
                                <li><a href="products-grid.html#" title="">$300  -  $400</a>
                                </li>
                                <li><a href="products-grid.html#" title="">$400  -  $500</a>
                                </li>
                                <li><a href="products-grid.html#" title="">$500  -  $600</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.widget -->


                    </div>

                </div>

                <div id="open-filters">
                    <i class="fa fa-filter"></i>
                    <span>Filter</span>
                </div>
            </div>
            <!-- /.col-* -->

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

    <script>
        $(function() { aweProductSidebar(); });
    </script>
    
@endsection
