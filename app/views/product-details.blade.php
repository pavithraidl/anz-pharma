<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/03/18 11:01.
 */
?>

@extends('master')

@section('content')

    <div class="main-header background background-image-heading-product" style="background: url('{{ URL::To('/') }}/assets/images/backgrounds/single-product.jpg'); background-position-y: -450px;">
        <div class="container">
            <h1 style="color: #00b4af; text-shadow: 1px 1px 2px rgba(0,0,0,0.6);">{{ $productDetails->name }}</h1>
        </div>
    </div>


    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ URL::Route('home') }}">Home</a></li>
                @if($productDetails->main_category_name != '<span style="font-style: italic; color: #89898975;">- None -</span>')
                    <li><a href="#">{{ $productDetails->main_category_name }}</a></li>
                @endif
                @if($productDetails->sub_category_name != '<span style="font-style: italic; color: #89898975;">- None -</span>')
                    <li><a href="#">{{ $productDetails->sub_category_name }}</a></li>
                @endif
                <li class="active"><span>{{ $productDetails->name }}</span></li>
            </ol>

        </div>
    </div>


    <div class="container">

        <div class="row">
            <div class="col-md-6">
                {{-- Product Image Slider --}}
                @if(sizeof($productDetails->img) > 0)
                    <?php
                        $mainImgUrl = VariableController::server.'/assets/backend/images/products/products/'.$productDetails->img[0]->imgid.'.'.$productDetails->img[0]->ext;
                    ?>
                    <img id="img_01" src="{{ $mainImgUrl }}" style="max-width: 400px;" data-zoom-image="{{ $mainImgUrl }}"/>
                    <div id="gal1">
                        @foreach($productDetails->img as $img)
                            <?php
                                $imgUrl = VariableController::server.'/assets/backend/images/products/products/'.$img->imgid.'.'.$img->ext;
                                $imgThumb = VariableController::server.'/assets/backend/images/products/products/'.$img->imgid.'_thumb.'.$img->ext;
                            ?>
                            <a href="#" data-image="{{ $imgUrl }}" data-zoom-image="{{ $imgUrl }}">
                                <img id="img_01" src="{{ $imgThumb }}" style="max-width: 80px; height: auto;" />
                            </a>
                        @endforeach
                    </div>
                @endif
                {{-- End Product Image Slider--}}
            </div>

            <div class="col-md-6">
                <nav class="pnav">
                    <div class="pull-right">
                        {{--<a href="#" class="btn btn-sm btn-arrow btn-default">--}}
                            {{--<i class="fa fa-chevron-left"></i>--}}
                        {{--</a>--}}

                        {{--<a href="#" class="btn btn-sm btn-arrow btn-default">--}}
                            {{--<i class="fa fa-chevron-right"></i>--}}
                        {{--</a>--}}
                    </div>
                    @if($productDetails->sub_category_name != '<span style="font-style: italic; color: #89898975;">- None -</span>')
                    <a href="#" class="back-to-pcate">
                        <i class="fa fa-chevron-left"></i>
                            <span>{{ $productDetails->sub_category_name }}</span>
                    </a>
                    @endif
                </nav>
                <!-- /header -->

                <div class="product-details-wrapper">
                    <h2 class="product-name">
                        <a href="#" title="{{ $productDetails->name }}"> {{ $productDetails->name }}</a>
                    </h2>
                    <!-- /.product-name -->

                    <div class="product-status">
                        <span>In Stock</span>
                        <span>-</span>
                        <small>SKU: 12345678</small>
                    </div>
                    <!-- /.product-status -->

                    <div class="product-stars">
                                <span class="rating">
                        <span class="star"></span>
                                <span class="star"></span>
                                <span class="star"></span>
                                <span class=""></span>
                                <span class=""></span>
                                </span>
                    </div>
                    <!-- /.product-stars -->

                    <div class="product-description">
                        <p>{{ $productDetails->small_description }}</p>
                    </div>
                    <!-- /.product-description -->

                    <div class="product-actions-wrapper">
                        <form action="product-quick-view.html" method="POST">
                            <div class="row">
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="p_color">Color</label>--}}
                                        {{--<select name="p_color" id="p_color" class="form-control">--}}
                                            {{--<option value="">Blue</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="p_size">Size</label>--}}
                                        {{--<select name="p_size" id="p_size" class="form-control">--}}
                                            {{--<option value="">300ml</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-3">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="p_qty">Qty</label>--}}
                                        {{--<select name="p_qty" id="p_qty" class="form-control">--}}
                                            {{--<option value="">1</option>--}}
                                            {{--<option value="">2</option>--}}
                                            {{--<option value="">3</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                        </form>
                        <!-- /.form -->

                        <div class="product-list-actions">
                            <span class="product-price">
                                <span class="amount">${{ $productDetails->price }}</span>
                                {{--<del class="amount">$8</del>--}}
                            </span>
                            <!-- /.product-price -->

                            <button class="btn btn-lg btn-primary">Add to cart</button>
                        </div>
                        <!-- /.product-list-actions -->
                    </div>
                    <!-- /.product-actions-wrapper -->

                    <div class="product-meta">
                                <span class="product-category">
                        <span>Category:</span>
                                <a href="#" title="">{{ $productDetails->sub_category_name }}</a>
                                </span>

                        <span>-</span>

                        <span class="product-tags">
                        <span>Brand:</span>
                                <a href="#" title=""><img style="width: 50px;" src="{{ VariableController::server }}assets/backend/images/products/manufactures/{{ $productDetails->manufacture_id }}/logo@100.{{ $productDetails->manufacture_ext }}" /> ({{ $productDetails->manufacture_name }})</a>
                                </span>
                    </div>
                    <!-- /.product-meta -->
                </div>
                <!-- /.product-details-wrapper -->
            </div>
        </div>

        <div class="product-socials">
            <ul class="list-socials">

                <li><a href="#" data-toggle="tooltip" title="Twitter"><i class="icon icon-twitter"></i></a>
                </li>
                <li><a href="#" data-toggle="tooltip" title="Facebook"><i class="icon icon-facebook"></i></a>
                </li>
                <li><a href="#" data-toggle="tooltip" title="Dot-Dot"><i class="icon icon-dot-dot"></i></a>
                </li>
                <li><a href="#" data-toggle="tooltip" title="Google+"><i class="icon icon-google-plus"></i></a>
                </li>
                <li><a href="#" data-toggle="tooltip" title="Pinterest"><i class="icon icon-pinterest"></i></a>
                </li>

            </ul>
        </div>
        <!-- /.product-socials -->

        <div class="product-details-left">
            <div role="tabpanel" class="product-details">
                <nav>
                    <ul class="nav" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#product-description" data-toggle="tab">Description</a>
                        </li>
                        <li role="presentation">
                            <a href="#product-infomation" data-toggle="tab">Additional Infomation</a>
                        </li>
                        <li role="presentation">
                            <a href="#product-review" data-toggle="tab">Review <span>(2)</span></a>
                        </li>
                    </ul>
                    <!-- /.nav -->
                </nav>
                <!-- /nav -->

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="product-description">
                        {{ $productDetails->description }}
                    </div>
                    <!-- /.tab-pane -->

                    <div role="tabpanel" class="tab-pane" id="product-infomation">
                        {{ $productDetails->additional_info }}
                    </div>
                    <!-- /.tab-pane -->

                    <div role="tabpanel" class="tab-pane" id="product-review">
                        <h3>Review <span>(2)</span></h3>

                        <ol class="product-review-list">
                            <li>
                                <h4 class="review-title">Goodale Rutledge Navy/White</h4>
                                <div class="rating small">
                                    <span class="star"></span>
                                    <span class="star"></span>
                                    <span class="star"></span>
                                    <span class="star"></span>
                                    <span class="star"></span>
                                </div>

                                <div class="review-comment">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                </div>

                                <div class="review-meta">
                                    <span>Posted by</span>
                                    <a href="#" class="author">Join Doe</a>
                                    <span>-</span>
                                    <span>February 17, 2015</span>
                                </div>
                            </li>

                            <li>
                                <h4 class="review-title">The standard Lorem Ipsum passage, used since the 1500s </h4>
                                <div class="rating small">
                                    <span class="star"></span>
                                    <span class="star"></span>
                                    <span class="star"></span>
                                    <span class="star"></span>
                                    <span class="star"></span>
                                </div>

                                <div class="review-comment">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                        mollit anim id est laborum.</p>
                                </div>

                                <div class="review-meta">
                                    <span>Posted by</span>
                                    <a href="#" class="author">Join Doe</a>
                                    <span>-</span>
                                    <span>February 17, 2015</span>
                                </div>
                            </li>
                        </ol>
                        <!-- /.product-review-list -->

                        <h3>Add a review</h3>

                        <form action="product-fullwidth.html" method="POST">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="reply-name">Name <sup>*</sup>
                                        </label>
                                        <input type="text" class="form-control" id="reply-name" placeholder="Name">
                                    </div>
                                    <!-- /.form-group -->
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="reply-email">Email <sup>*</sup>
                                        </label>
                                        <input type="email" class="form-control" id="reply-email" placeholder="Email">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="reply-title">Title <sup>*</sup>
                                </label>
                                <input type="text" class="form-control" id="reply-title" placeholder="title">
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label for="reply-text">Your review <sup>*</sup>
                                </label>
                                <textarea name="reply-text" class="form-control" id="reply-text" rows="7" placeholder="Your review"></textarea>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-submit clearfix">
                                <div class="review-rating">
                                    <span class="title">Your rating:</span>

                                    <span class="rating small live">
                                    <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            </span>

                                </div>

                                <div class="pull-right">
                                    <button type="submit" class="submit btn btn-lg btn-default">Submit</button>
                                </div>
                            </div>
                            <!-- /.form-submit -->
                        </form>
                        <!-- /form -->
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.product-details-left -->

        <div class="relared-products">
            <div class="relared-products-header margin-bottom-50">
                <h3 class="upper">Related Products</h3>
            </div>

            <div class="col-md-12">
                <div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="myCarousel">
                    <div class="carousel-inner">
                        @foreach($productDetails->related_products as $relatedProduct)
                        <?php
                            if(sizeof($relatedProduct->img) > 0)
                            $imgUrl = VariableController::server.'/assets/backend/images/products/products/'.$relatedProduct->img[0]->imgid.'.'.$relatedProduct->img[0]->ext;
                            else
                            $imgUrl = URL::To('/').'/assets/backend/images/products/products/default.png';

                            $active = 'active';
                        ?>
                            <div class="item {{ $active }}">
                                <div class="col-md-3">
                                    <div class="product product-grid">
                                        <div class="product-media">
                                            <div class="product-thumbnail">
                                                <a href="{{ URL::To('/') }}/product/details/{{ $relatedProduct->id }}" title="">
                                                    <img src="{{ $imgUrl }}" alt="" class="current">
                                                </a>
                                            </div>
                                            <!-- /.product-thumbnail -->
                                            <div class="product-hover">
                                                <div class="product-actions">
                                                    <a href="#" class="awe-button product-add-cart" data-toggle="tooltip" title="Add to cart">
                                                        <i class="icon icon-shopping-bag"></i>
                                                    </a>

                                                    <a href="#" class="awe-button product-quick-whistlist" data-toggle="tooltip" title="Add to whistlist">
                                                        <i class="icon icon-star"></i>
                                                    </a>

                                                    <a href="javascript:void(0);" onclick="quickView({{ $relatedProduct->id }})" class="awe-button product-quick-view" data-toggle="tooltip" title="Quickview">
                                                        <i class="icon icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- /.product-hover -->
                                            <span class="product-label hot">
                                                <span>Famous</span>
                                            </span>
                                        </div>
                                        <!-- /.product-media -->
                                        <div class="product-body">
                                            <h2 class="product-name">
                                                <a href="{{ URL::To('/') }}/product/details/{{ $relatedProduct->id }}" title="Gin Lane Greenport Cotton Shirt">{{ $relatedProduct->name }}</a>
                                            </h2>
                                            <!-- /.product-product -->
                                            <div class="product-category">
                                                <span>{{ $relatedProduct->sub_category }}</span>
                                            </div>
                                            <!-- /.product-category -->
                                            <div class="product-price">
                                                <span class="amount">${{ $relatedProduct->price }}</span>
                                            </div>
                                            <!-- /.product-price -->
                                        </div>
                                        <!-- /.product-body -->
                                    </div>
                                </div>
                            </div>
                            <?php $active = '' ?>
                        @endforeach
                        <!-- /.product -->

                    </div>
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                </div>
            </div>
        </div>
        <!-- /.relared-products -->
    </div>

    <script>
        $(function() { aweProductRender(true); });

        //initiate the plugin and pass the id of the div containing gallery images
        $("#img_01").elevateZoom({gallery:'gal1', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'});

        //pass the images to Fancybox
        $("#img_01").bind("click", function(e) {
            var ez =   $('#img_01').data('elevateZoom');
            $.fancybox(ez.getGalleryList());
            return false;
        });

        $('.carousel[data-type="multi"] .item').each(function(){
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            for (var i=0;i<4;i++) {
                next=next.next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }

                next.children(':first-child').clone().appendTo($(this));
            }
        });
    </script>

@endsection
