<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/03/18 09:50.
 */
?>

@extends('master')

@section('content')

    <div class="main-header background background-image-heading-about-us" style="background-image: url('{{ $contentObject['50']['89']['top_banner_image_url'] }}');">
        <div class="container">
            <h1 style="color: rgb(0, 193, 187); text-shadow: 1px 1px 2px rgba(0,0,0,0.66);">{{ $contentObject['50']['89']['banner_text'] }}</h1>
        </div>
    </div>


    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{URL::Route('home')}}">Home</a>
                </li>
                <li class="active"><span>About Us</span>
                </li>
            </ol>

        </div>
    </div>


    <section>
        <div class="container">

            <div class="padding-vertical-50 border-bottom">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <div class="awe-box center">
                            <div class="awe-box-media">
                                <div class="awe-box-image">
                                    <img src="{{ $contentObject['51']['90']['paragraph_top_image_url'] }}" alt="">
                                </div>
                            </div>
                            <!-- /.awe-box-media -->

                            <div class="awe-box-content">
                                {{ $contentObject['51']['90']['content'] }}
                            </div>
                            <!-- /.awe-box-content -->
                        </div>
                        <!-- /.awe-box -->

                    </div>
                </div>
            </div>
            <!-- /.padding-vertical-50 -->

            <div class="padding-vertical-50 border-bottom">
                <div class="row">

                    @foreach($contentObject['52'] as $block)
                        <div class="col-md-4 col-sm-4">
                            <div class="awe-box center box-hover margin-bottom-25">
                                <div class="awe-box-media">
                                    <div class="awe-box-icon icon-large">
                                        <i class="fa fa-{{ $block['icon'] }}"></i>
                                    </div>
                                </div>
                                <!-- /.awe-box-media -->

                                <div class="awe-box-content">
                                    <h3>{{ $block['title'] }}</h3>
                                    <p>{{ $block['content'] }}</p>
                                </div>
                                <!-- /.awe-box-content -->
                            </div>
                            <!-- /.awe-box -->
                        </div>
                    @endforeach
                </div>
                <!-- /.row -->
            </div>
            <!-- /.padding-vertical-50 -->

            <div class="padding-vertical-80">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <blockquote class="blockquote large center">
                            <p>{{ $contentObject['53']['94']['quote'] }}</p>
                            <footer>
                                <h4>{{ $contentObject['53']['94']['name'] }}</h4>
                                <span>{{ $contentObject['53']['94']['title'] }}</span>
                            </footer>
                        </blockquote>
                    </div>
                </div>
            </div>
            <!-- /.padding-vertical-80 -->

        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section class="background background-color-dark background-image-section-counting background-fixed" style="background-image: url('{{URL::To('/')}}/assets/images/backgrounds/about-us-counting.jpg');">
        <div class="container">

            <div class="padding-vertical-100">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="awe-counting">
                            <h4>Total Selling Item</h4>
                            <span>1 234</span>
                        </div>
                    </div>
                    <!-- /.awe-counting -->

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="awe-counting">
                            <h4>Supply Chain</h4>
                            <span>1 560</span>
                        </div>
                        <!-- /.awe-counting -->
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="awe-counting">
                            <h4>Total Customers</h4>
                            <span>724</span>
                        </div>
                        <!-- /.awe-counting -->
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="awe-counting">
                            <h4>Total Users</h4>
                            <span>11 154</span>
                        </div>
                        <!-- /.awe-counting -->
                    </div>
                </div>
                <!-- /.row -->
            </div>

        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section>
        <div class="container">
            <div class="padding-top-80">

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-header center">
                            <h2 class="medium margin-bottom-40">Meet our team</h2>
                            <p class="medium">Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla. Donec a neque
                                libero. Pellentesque aliquet, sem eget</p>
                        </div>
                        <!-- /.section-header -->
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">

                    @foreach($contentObject['54'] as $team)
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="about-us-team awe-media margin-bottom-50">
                            <div class="awe-media-header">
                                <div class="awe-media-image">
                                    <img src="{{ $team['image_url'] }}" alt="">
                                </div>
                                <!-- /.awe-media-image -->

                                <div class="awe-media-hover dark fullpage">
                                    <div class="fp-table">
                                        <div class="fp-table-cell center">
                                            <h6 class="upper margin-bottom-20">Connect</h6>
                                            <ul class="list-socials list-large list-light">

                                                @if($team['twitter_link'] != '' && $team['twitter_link'] != '#')
                                                    <li><a href="{{ $team['twitter_link'] }}" data-toggle="tooltip" title="Twitter"><i class="icon icon-twitter"></i></a></li>
                                                @endif
                                                @if($team['facebook_link'] != '' && $team['facebook_link'] != '#')
                                                    <li><a href="{{ $team['facebook_link'] }}" data-toggle="tooltip" title="Facebook"><i class="icon icon-facebook"></i></a></li>
                                                @endif
                                                @if($team['google+_link'] != '' && $team['twitter_link'] != '#')
                                                    <li><a href="{{ $team['google+_link'] }}" data-toggle="tooltip" title="Google+"><i class="icon icon-google-plus"></i></a></li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.awe-media-hover -->
                            </div>
                            <!-- /.awe-media-header -->

                            <div class="awe-media-body center">
                                <h3 class="awe-media-title text-upper">
                                    <a href="#" title="Geoff weiss">{{ $team['name'] }}</a>
                                </h3>

                                <p class="awe-media-caption">
                                    <span class="bold">{{ $team['title'] }}</span>
                                </p>
                            </div>
                            <!-- /.awe-media-body -->
                        </div>
                        <!-- /.awe-media -->
                    </div>
                    @endforeach

                </div>
                <!-- /.row -->

            </div>
            <!-- /.padding-top-80 -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    
@endsection
