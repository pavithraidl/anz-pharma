<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 19/03/18 12:31.
 */
?>

@extends('master')

@section('content')
    <div class="main-header background background-image-heading-about-us" style="">
        <div class="container">
            <h1 style="color: rgb(0, 193, 187); text-shadow: 1px 1px 2px rgba(0,0,0,0.66);">{{ $pageName }}</h1>
        </div>
    </div>


    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{URL::Route('home')}}">Home</a>
                </li>
                <li class="active"><span>Terms of Use</span>
                </li>
            </ol>

        </div>
    </div>


    <section>
        <div class="container">

            <div class="border-bottom">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        {{ $contentObject['55']['101']['content'] }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
