<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 25/04/18 07:19.
 */
?>

@extends('master')

@section('styles')
    {{ HTML::style('assets/css/brands.css') }}
@endsection

@section('content')

    <div class="main-header background background-image-heading-products" style="background: url('{{ $contentObject[57][103]['banner_image_url'] }}');">
        <div class="container">
            <h1 style="text-align: center; margin-top: -25px; padding-bottom: 23px;">{{ $contentObject[57][103]['banner_title'] }}</h1>
        </div>
    </div>


    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ URL::Route('home') }}">Home</a></li>
                <li class="active"><span>Brands</span></li>
            </ol>

        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group col-md-4 col-sm-6 col-xs-8 pull-right">
                    <input type="text" class="form-control" style="border-radius: 15px; margin-top: -60px;" id="search-brands" placeholder="Type Here To Search Brands" />
                </div>
            </div>
            <div class="col-xs-12 loading" style="position: relative;">
                <div class="brand-container">
                    {{--JQuery Append--}}
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>


@endsection

@section('scripts')
    {{ HTML::script('assets/js/brands.js') }}
@endsection