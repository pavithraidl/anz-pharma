<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/03/18 10:11.
 */
?>

@extends('master')

@section('content')

    <div class="main-header background background-image-heading-contact">
        <div class="container">
            <h1>Contact</h1>
        </div>
    </div>


    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li class="active"><span>Contact</span>
                </li>
            </ol>

        </div>
    </div>


    <div class="contact-wrapper">
        <div class="margin-bottom-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="contact-content">
                            <div class="contact-header">
                                <div class="contact-image">
                                    <iframe src="{{ $contentObject['56']['102']['google_map_link'] }}" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <!-- /.contact-header -->

                            <div class="contact-block">
                                <h3>STORE INFOMATION</h3>

                                <dl class="dl-horizontal">
                                    <dt>Address</dt>
                                    <dd>{{ str_replace(',', ', <br/>', $contentObject['56']['102']['address']) }}<br/><br/></dd>

                                    <dt>Phone</dt>
                                    <dd> <a href="tel:{{ $contentObject['56']['102']['phone'] }}">{{ $contentObject['56']['102']['phone'] }}</a></dd>

                                    <dt>Email</dt>
                                    <dd><a href="mailto:{{ $contentObject['56']['102']['email'] }}">{{ $contentObject['56']['102']['email'] }}</a></dd>
                                </dl>
                            </div>
                            <!-- /.contact-block -->

                            <div class="contact-block">
                                {{ $contentObject['56']['102']['customer_service_hours'] }}
                            </div>
                            <!-- /.contact-block -->
                        </div>
                        <!-- /.contact-content -->
                    </div>
                    <!-- /.col-md-6 -->

                    <div class="col-md-6">
                        <div class="contact-content">
                            <div class="contact-form-heading">
                                <h2>EMAIL TO OUR</h2>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                            </div>
                            <!-- /.contact-content -->

                            <div id="ajax-message"></div>

                            <form action="#" method="POST" id="contact-form">
                                <div class="form-group">
                                    <label for="contact-name">Name</label>
                                    <input type="text" name="name" id="contact-name" class="form-control dark" required>
                                </div>
                                <!-- /.form-group -->

                                <div class="form-group">
                                    <label for="contact-email">Email</label>
                                    <input type="email" name="email" id="contact-email" class="form-control dark" required>
                                </div>
                                <!-- /.form-group -->

                                <div class="form-group">
                                    <label for="contact-website">Website</label>
                                    <input type="url" name="website" id="contact-website" class="form-control dark">
                                </div>
                                <!-- /.form-group -->

                                <div class="form-group">
                                    <label for="contact-message">Your comment</label>
                                    <textarea name="message" id="contact-message" class="form-control dark" rows="7" required></textarea>
                                </div>
                                <!-- /.form-group -->

                                <div class="form-button">
                                    <button type="submit" class="btn btn-lg btn-dark">Send Message</button>
                                </div>
                                <!-- /.form-button -->
                            </form>
                        </div>
                    </div>
                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.margin-bottom-100 -->

    </div>
    <!-- /.contact-wrapper -->

@endsection
