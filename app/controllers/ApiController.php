<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {13/04/18} {08:50}.
 */

class ApiController extends BaseController
{

    public function getPopularCategories()
    {

        $paginate = Input::get('paginate');
        $perPage = Input::get('perpage');

        $url = VariableController::server.'website/getpopularcategories';
        $ch = curl_init($url);


        $jsonData = array(
            'perpage' => $perPage,
            'paginate' => $paginate
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return json_encode($result['data']);
    }

    public function getNewProducts() {
        $paginate = Input::get('paginate');
        $perPage = Input::get('perpage');

        $url = VariableController::server.'website/getnewproducts';
        $ch = curl_init($url);


        $jsonData = array(
            'perpage' => $perPage,
            'paginate' => $paginate
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return json_encode($result['data']);
    }


    public function getFeaturedProducts() {
        $paginate = Input::get('paginate');
        $perPage = Input::get('perpage');

        $url = VariableController::server.'website/getfeaturedproducts';
        $ch = curl_init($url);


        $jsonData = array(
            'perpage' => $perPage,
            'paginate' => $paginate
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return json_encode($result['data']);
    }

    function getProductDetails() {
        $productId = Input::get('productid');

        $url = VariableController::server.'website/getproductdetails';
        $ch = curl_init($url);


        $jsonData = array(
            'productid' => $productId
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return json_encode($result['data']);
    }

    function getProductFullDetails($productId) {

        $url = VariableController::server.'website/getproductfulldetails';
        $ch = curl_init($url);


        $jsonData = array(
            'productid' => $productId
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return json_encode($result['data']);
    }

    function getManufactureDetails($manufactureId) {
        $url = VariableController::server.'website/getmanufacturedetails';
        $ch = curl_init($url);


        $jsonData = array(
            'manufactureid' => $manufactureId
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return json_encode($result['data']);
    }

    function getManufactureProducts() {

        $manufactureId = Input::get('id');
        $paginate = Input::get('paginate');
        $sort = Input::get('sort');
        $orderBy = 'views';
        $orderType = 'ASC';

        $url = VariableController::server.'website/getmanufactureproducts';
        $ch = curl_init($url);

//        if($sort == 2) {
//            $orderBy = 'price';
//            $orderType = 'ASC';
//        }
//        if($sort == 3) {
//            $orderBy = 'price';
//            $orderType = 'DESC';
//        }


        $jsonData = array(
            'manufactureid' => $manufactureId,
            'perpage' => 40,
            'paginate' => $paginate,
            'orderby' => $orderBy,
            'ordertype' => $orderType
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return json_encode($result['data']);
    }

    function getManufactures() {
        $paginate = Input::get('paginate');

        $url = VariableController::server.'website/getmanufactures';
        $ch = curl_init($url);


        $jsonData = array(
            'paginate' => $paginate
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return json_encode($result['data']);
    }

    function getCategoryDetails($categoryId) {
        $url = VariableController::server.'website/getcategorydetails';
        $ch = curl_init($url);


        $jsonData = array(
            'categoryid' => $categoryId
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return json_encode($result['data']);
    }

    function getCategoryProducts() {

        $categoryId = Input::get('id');
        $subCategoryId = Input::get('sub-categoryid');
        $paginate = Input::get('paginate');
        $sort = Input::get('sort');
        $perPage = Input::get('perpage');
        $orderBy = 'id';
        $orderType = 'ASC';

        $url = VariableController::server.'website/getcategoryproducts';
        $ch = curl_init($url);

//        if($sort == 2) {
//            $orderBy = 'price';
//            $orderType = 'ASC';
//        }
//        if($sort == 3) {
//            $orderBy = 'price';
//            $orderType = 'DESC';
//        }


        $jsonData = array(
            'categoryid' => $categoryId,
            'sub-categoryid' => $subCategoryId,
            'perpage' => $perPage,
            'paginate' => $paginate,
            'orderby' => $orderBy,
            'ordertype' => $orderType
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return json_encode($result['data']);
    }
}