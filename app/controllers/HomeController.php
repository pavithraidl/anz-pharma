<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getHome() {
	    $sliderIds = [46, 48, 49, 56];
	    $contentObject = $this->getWebsiteContent($sliderIds);
	    $menu = $this->getMenu();

	    $pageName = 'Home';
        $this->setupPageDefaults($pageName);


		return View::make('default', array(
		    'pageName' => $pageName,
            'orgId' => VariableController::orgId,
            'contentObject' => $contentObject['data'],
            'menu' => $menu['data']
        ));
	}

	public function getAboutUs() {
	    $sliderIds = [50, 51, 52, 53, 54, 56];
	    $contentObject = $this->getWebsiteContent($sliderIds);
        $menu = $this->getMenu();

        $pageName = 'About Us';
        $this->setupPageDefaults($pageName);

	    return View::make('about-us', array(
	        'pageName' => 'About Us',
            'contentObject' => $contentObject['data'],
            'menu' => $menu['data']
        ));
    }

    public function getContact() {
	    $sliderIds = [56];
	    $contentObject = $this->getWebsiteContent($sliderIds);
        $menu = $this->getMenu();

        $pageName = 'Contacts';
        $this->setupPageDefaults($pageName);

	    return View::make('contact', array(
	        'pageName' => $pageName,
            'contentObject' => $contentObject['data'],
            'menu' => $menu['data']
        ));
    }

    public function getBrands() {
        $sliderIds = [56, 57];
        $contentObject = $this->getWebsiteContent($sliderIds);
        $menu = $this->getMenu();

        $pageName = 'Brands';
        $this->setupPageDefaults($pageName);

        return View::make('brands', array(
            'pageName' => $pageName,
            'contentObject' => $contentObject['data'],
            'menu' => $menu['data']
        ));
    }

    public function getManufactureGridView($manufactureId) {
        $sliderIds = [56];
        $contentObject = $this->getWebsiteContent($sliderIds);
        $menu = $this->getMenu();

        $api = new ApiController();
        $pageData = json_decode($api -> getManufactureDetails($manufactureId));

        $pageName = $pageData->name;
        $this->setupPageDefaults($pageName);

	    return View::make('products-grid-view', array(
	        'pageName' => $pageName,
            'pageType' => '1',
            'pageId' => $manufactureId,
            'contentObject' => $contentObject['data'],
            'menu' => $menu['data'],
            'pageData' => $pageData
        ));
    }

    public function getCategoryGridView($categoryId) {
        $sliderIds = [56];
        $contentObject = $this->getWebsiteContent($sliderIds);
        $menu = $this->getMenu();

        $api = new ApiController();
        $pageData = json_decode($api -> getCategoryDetails($categoryId));

        $pageName = $pageData->name;
        $this->setupPageDefaults($pageName);

        return View::make('products-grid-view', array(
            'pageName' => $pageName,
            'pageType' => '2',
            'pageId' => $categoryId,
            'contentObject' => $contentObject['data'],
            'menu' => $menu['data'],
            'pageData' => $pageData
        ));
    }

    public function getListView() {
        $sliderIds = [56];
        $contentObject = $this->getWebsiteContent($sliderIds);
        $menu = $this->getMenu();

        return View::make('products-list-view', array(
            'pageName' => 'Products',
            'contentObject' => $contentObject['data'],
            'menu' => $menu['data']
        ));
    }

    public function getDetails($productId) {
        $sliderIds = [56];
        $contentObject = $this->getWebsiteContent($sliderIds);
        $menu = $this->getMenu();

        $api = new ApiController();
        $productDetails = $api->getProductFullDetails($productId);
        $productDetails = json_decode($productDetails);
        $productName = $productDetails->name;
        $this->setupPageDefaults($productName);

//        print_r($productDetails);exit;

	    return View::make('product-details', array(
	        'pageName' => $productName,
            'contentObject' => $contentObject['data'],
            'menu' => $menu['data'],
            'productDetails' => $productDetails
        ));
    }

    public function getBlog() {
        $sliderIds = [56];
        $contentObject = $this->getWebsiteContent($sliderIds);
        $menu = $this->getMenu();

        $pageName = 'Blog';
        $this->setupPageDefaults($pageName);

        return View::make('blog', array(
            'pageName' => $pageName,
            'contentObject' => $contentObject['data'],
            'menu' => $menu['data']
        ));
    }

    public function getBlogPost() {
        $sliderIds = [56];
        $contentObject = $this->getWebsiteContent($sliderIds);
        $menu = $this->getMenu();

        return View::make('single-blog', array(
            'pageName' => 'Blog Post',
            'contentObject' => $contentObject['data'],
            'menu' => $menu['data']
        ));
    }

    public function getTerms() {
	    $sliderIds = [55, 56];
	    $contentObject = $this->getWebsiteContent($sliderIds);
        $menu = $this->getMenu();

	    return View::make('term', array(
	        'pageName' => 'Terms of Use',
            'contentObject' => $contentObject['data'],
            'menu' => $menu['data']
        ));
    }

    public function getWebsiteContent($sliderIds) {

        $url = VariableController::server.'website/getsliders';
        $ch = curl_init($url);


        $jsonData = array(
            'sliderids' => $sliderIds
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return $result;
    }

    public function getMenu() {
        $url = VariableController::server.'website/getmenu';
        $ch = curl_init($url);


        $jsonData = array();

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Org-Key: '.VariableController::key,
            'Org-Secret: '.VariableController::secret
        ));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return $result;
    }

    public function setupPageDefaults($pageName) {
//	    if(Session::has('current_page_name') && Session::has('current_page_url')) {
//	        if(Session::get('current_page_name') != Session::get('old_page_name')) {
//                if(Session::has('current_page_name')) {
//                    $oldPageName = Session::get('current_page_name');
//                    Session::put('old_page_name', $oldPageName);
//                }
//                if(Session::has('current_page_url')) {
//                    $oldPageUrl = Session::get('current_page_url');
//                    Session::put('old_page_url', $oldPageUrl);
//                }
//            }
//
//        }
//
//
//        Session::put('current_page_name', $pageName);
//	    Session::put('current_page_url', Request::url());
    }

}
