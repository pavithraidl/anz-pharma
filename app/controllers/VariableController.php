<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {16/03/18} {21:15}.
 */

class VariableController extends BaseController {

    const orgId = 8;
    const key = '4pBW2esw6GbmCPcUBT8000JguggS8N1ku96O';
    const secret = 'tA2I1qEPssxei4NC7a1a85s7YCLgjZZesYNS28isjINlHthOzYGRVZ0PC2A6';
    const server = 'https://goplaza.nz/';
    const stagingExtenstion = '';
    const version = '1.0';

    public function getVariable($name) {
        try {
            $domain = URL::to('/').'/'; //website domain

            //returning relevant variable
            switch ($name) {
                case "orgid":
                    return self::orgId;
                    break;
                case "key":
                    return self::key;
                    break;
                case "secret":
                    return self::secret;
                    break;
                case "server":
                    return self::server;
                    break;
                case "domain":
                    return $domain;
                    break;

                case "version":
                    return self::version;
                    break;

                case 'staging-extension':
                    return self::stagingExtenstion;
                    break;
            }

        } Catch(Exception $ex) {
            return 0;
        }
    }
}