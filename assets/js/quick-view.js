/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 20/04/18 00:47.
 */

"use strict";
window.productQuickViewPopupActive = 0;

function quickView(productId) {
    $('.product-quickview-popup-container').removeClass('pre-loading').addClass('loading');
    $('.product-quickview-slider').owlCarousel('destroy');

    //ajax method
    $.ajax({
        type: 'GET',
        url: window.domain + 'api/get-product-details',
        data: {productid:productId}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {

                var res = JSON.parse(data);
                var imgs = '';

                for(var i = 0; i < res.img.length; i++) {
                    imgs += '<div class="owl-item">' +
                        '       <img src="'+window.server+'assets/backend/images/products/products/'+res.img[i].imgid+'.'+res.img[i].ext+'" alt="">' +
                        '   </div>';
                }
                var owl = $(".product-quickview-slider");
                owl.html(imgs);

                $('#quick-view-product-name').text(res.name).attr('href', window.domain+'product/details/'+productId);
                $('#quick-view-product-id').text(res.external_id);
                $('#quick-view-product-price').text('$'+res.price);
                $('#quick-view-product-category').html(res.sub_category);
                $('#quck-view-product-manufacture').text(res.manufacture_name).attr('href', window.domain+'products/brand/'+res.manufacture_id);
                $('#quick-view-product-small-description').html(res.small_description);

                $('.product-quickview-slider').owlCarousel({
                    items: 1,
                    nav: true,
                    dots: false
                });



                setTimeout(function () {
                    $('.product-quickview-popup-container').removeClass('loading').addClass('loaded');
                    window.productQuickViewPopupActive = 1;
                }, 120);

            }
        },
        error: function (xhr, textShort, errorThrown) {
            console.log(xhr);
            console.log(textShort);
            console.log(errorThrown);
        }
    });
}

$(document).mouseup(function(e)
{
    var container = $(".product-quickview-popup");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        if(window.productQuickViewPopupActive == 1)
            closeProductQuickView();
    }
});

function closeProductQuickView() {
    $('.product-quickview-popup-container').removeClass('loaded').addClass('exit');

    setTimeout(function () {
        $('.product-quickview-popup-container').removeClass('exit').addClass('pre-loading');
        window.productQuickViewPopupActive = 0;
    }, 1200);
}