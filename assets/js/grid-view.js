/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 23/04/18 13:47.
 */

"use strict";
window.paginateOn = 0;
window.paginateDisabled = 0;
window.totalItemsDisplayed = 0;
window.sort = 1;

$(document).ready(function () {
    window.paginate = 1;
    getProducts();
    setTimeout(function () {
        window.paginateOn = 1;
    }, 500);
});

$(window).scroll(function () {
    var elementHeight = $('#product-container')[0].scrollHeight+396;
    var scrollPosition = $(window).height() + $(window).scrollTop();

    if (elementHeight -1000 < scrollPosition) {
        if(window.paginateOn == 1 && window.paginateDisabled == 0) {
            window.paginateOn = 0;
            window.paginate++;
            getProducts();
        }

    }
});

function getProducts() {

    var perPage = 40;

    var url = '';
    var productClass = 'product-item';

    if(window.paginate != 1) {
        $('#product-container').parent().removeClass('loaded-more').addClass('loading-more');
        productClass = 'more-product-item';
    }

    if(window.pageType == 1) {
        url = 'api/get-manufacture-products';
    }
    if(window.pageType == 2) {
        url = 'api/get-category-products';
    }
    //ajax method
    jQuery.ajax({
        type: 'GET',
        url: window.domain + url,
        data: {id:window.pageId, paginate:window.paginate, sort:window.sort, perpage:perPage}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);
                var products = '';
                var mainImgUrl = window.server+'assets/backend/images/products/products/default.png';
                var hoverImgUrl, categoryBlockClass, label = '';

                if(res.products.length > 0) {
                    for(var i = 0; i < res.products.length; i++) {

                        for(var j = 0; j < res.products[i].img.length; j++) {
                            if(res.products[i].img[j].type == 1) {
                                mainImgUrl = window.server+'assets/backend/images/products/products/'+res.products[i].img[j].imgid+'.'+res.products[i].img[j].ext;
                            }
                            else if(res.products[i].img[j].type == 2) {
                                hoverImgUrl = window.server+'assets/backend/images/products/products/'+res.products[i].img[j].imgid+'.'+res.products[i].img[j].ext;
                            }
                        }

                        if(res.products[i].new == 2) {
                            label ='<span class="product-label new">' +
                                '       <span>new</span>' +
                                '   </span>';
                        }

                        products += '<div class="col-md-3 col-sm-4 col-xs-12 '+productClass+'">' +
                            '                            <div id="product-block-'+res.products[i].id+'" class="product product-grid">' +
                            '                                <div class="product-media">' +
                            '                                    <div class="product-thumbnail">' +
                            '                                       <a href="'+window.domain+'product/details/'+res.products[i].id+'" title="'+res.products[i].name+'">' +
                            '                                           <img src="'+mainImgUrl+'" alt="'+res.products[i].name+' Product Image" class="current">' +
                            '                                       </a>' +
                            '                                    </div>' +
                            '                                    <!-- /.product-thumbnail -->' +
                            '                                    <div class="product-hover">' +
                            '                                        <div class="product-actions">' +
                            '                                            <a href="#" class="awe-button product-add-cart" data-toggle="tooltip" title="Add to cart">' +
                            '                                                <i class="icon icon-shopping-bag"></i>' +
                            '                                            </a>' +
                            '                                            <a href="#" class="awe-button product-quick-whistlist" data-toggle="tooltip" title="Add to whistlist">' +
                            '                                                <i class="icon icon-star"></i>' +
                            '                                            </a>' +
                            '                                            <a href="javascript:void(0);" onclick="quickView('+res.products[i].id+');" class="awe-button product-quick-view" data-toggle="tooltip" title="Quickview">' +
                            '                                                <i class="icon icon-eye"></i>' +
                            '                                            </a>' +
                            '                                        </div>' +
                            '                                    </div>' +
                            '                                    <!-- /.product-hover -->' +
                            '                                   ' +label+
                            '                                </div>' +
                            '                                <!-- /.product-media -->' +
                            '                                <div class="product-body">' +
                            '                                    <h2 class="product-name">' +
                            '                                       <a href="'+window.domain+'product/details/'+res.products[i].id+'" title="'+res.products[i].name+'">'+res.products[i].name+'</a>' +
                            '                                   </h2>' +
                            '                                    <!-- /.product-product -->' +
                            '                                    <div class="product-category">' +
                            '                                        <span>'+res.products[i].sub_category+'</span>' +
                            '                                    </div>' +
                            '                                    <!-- /.product-category -->' +
                            '                                    <div class="product-price">' +
                            '                                        <span class="amount">$'+res.products[i].price+'</span>' +
                            '                                    </div>' +
                            '                                    <!-- /.product-price -->' +
                            '                                </div>' +
                            '                                <!-- /.product-body -->' +
                            '                            </div>' +
                            '                            <!-- /.product -->' +
                            '                        </div>';

                        window.totalItemsDisplayed++;
                    }

                    $('#product-container').append(products)
                        .removeClass('loading')
                        .addClass('loaded');
                    $('#item-count').text('Item 1 to '+window.totalItemsDisplayed+' of '+res.total+' Items');

                    if(window.paginate != 1) {
                        $('#product-container').parent().removeClass('loading-more').addClass('loaded-more');
                        $('.more-product-item').removeClass('more-product-item');
                    }
                    window.paginateOn = 1;
                }
                else {
                    console.log('No products - paginate disabled');
                    window.paginateDisabled = 1;
                    $('.loader').addClass('no-more').removeClass('loader').html('<p style="text-align: center">No More...</p>');
                }

                if(res.products.length < perPage) {
                    window.paginateDisabled = 1;
                }


            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function sortProducts() {
    var sort = $('#p_sort_by').val();
    $('#product-container').html('').addClass('loading').removeClass('loaded');
    window.paginateOn = 0;
    window.paginateDisabled = 0;
    window.totalItemsDisplayed = 0;
    $('#item-count').text('Item 0 to 0 of loading Items');
    getProducts();
}