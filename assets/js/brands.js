/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 25/04/18 08:11.
 */

"use strict";
window.paginate = 1;
window.paginateOn = 0;
window.paginateDisabled = 0;

$(document).ready(function () {
    getBrands(); 
});

$(window).scroll(function () {
    var elementHeight = $('.brand-container').parent()[0].scrollHeight+396;
    var scrollPosition = $(window).height() + $(window).scrollTop();

    if (elementHeight -1200 < scrollPosition) {
        if(window.paginateOn == 1 && window.paginateDisabled == 0) {
            window.paginateOn = 0;
            window.paginate++;
            getBrands();
        }
    }
});


function getBrands() {
    if(window.paginate != 1) {
        $('.brand-container').append('<div class="loader col-md-12"></div>');
        $('.brand-container').parent().addClass('loading-more').removeClass('loaded-more');
    }
    //ajax method
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'api/get-manufactures',
        data: {paginate: window.paginate}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {

                var res = JSON.parse(data);
                var brands = '';


                if(res.length > 0) {
                    for(var i = 0; i < res.length; i++) {
                        brands += '      <div class="col-md-3 col-sm-6 col-xs-6 brand-items">' +
                            '                    <div class="awe-media">' +
                            '                        <div class="awe-media-image">' +
                            '                            <a href="'+window.domain+'products/brand/'+res[i].id+'" title="ANZ Pharma Brands '+res[i].name+'">' +
                            '                                <img src="'+res[i].img200_url+'" alt="">' +
                            '                            </a>' +
                            '                        </div>' +
                            '                       <div class="awe-media-body center margin-vertical-20">' +
                            '                            <a href="'+window.domain+'products/brand/'+res[i].id+'" title="New Fashion for Cafe Racer Boy">'+res[i].name+'</a>' +
                            '                        </div>' +
                            '                    </div>' +
                            '                </div>';
                    }

                    $('.brand-container').append(brands);
                    if(window.paginate != 1) {
                        $('.brand-container').parent().addClass('loaded-more').removeClass('loading-more');
                    }
                    else {
                        $('.brand-container').parent().addClass('loaded').removeClass('loading');
                    }


                    setTimeout(function () {
                        $('.brand-items').removeClass('brand-items');
                        $('.loader').remove();
                    }, 310);

                    if(res.length < 40) {
                        window.paginateDisabled = 1;
                    }
                    window.paginateOn = 1;
                }
                else {
                    window.paginateDisabled = 1;
                }

            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}