/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 12/04/18 11:49.
 */

"use strict";
window.categoryPaginate = 2;
window.newProductPaginate = 2;
window.featuredProductPaginate = 2;

$(document).ready(function () {
    getPopularCategories(1, 4);
    getNewProducts(1, 4);
    getFeaturedProducts(1, 16);
});

function showMoreProductCategories() {
    var paginate = window.categoryPaginate;
    getPopularCategories(categoryPaginate, 8);
    window.categoryPaginate++;
}

function getPopularCategories(paginate, perPage) {

    if(paginate != 1) {
        $('.popular-category-show-more-container').removeClass('loaded').addClass('loading-ring').html('');
    }
    //ajax method
    $.ajax({
        type: 'GET',
        url: window.domain + 'api/get-popular-categories',
        data: {paginate:paginate, perpage:perPage}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var appendHtml = '';
                var res = JSON.parse(data);
                var imgUrl = window.server+'assets/backend/images/products/products/default.png';
                var categoryBlockClass = '';

                for(var i = 0; i < res.length; i++) {
                    if(res[i].img == 1) {
                        imgUrl = window.server+'assets/backend/images/products/categories/'+window.orgId+'/'+res[i].id+'.'+res[i].ext;
                    }
                    if(paginate != 1) {
                        categoryBlockClass = 'category-block'
                    }
                    appendHtml += '<div class="col-md-3 col-sm-6 col-xs-6 '+categoryBlockClass+'">' +
                        '               <div class="awe-media">' +
                        '                   <div class="awe-media-image">' +
                        '                       <a href="#" title="'+res[i].name+'">' +
                        '                           <img src="'+imgUrl+'" alt="">' +
                        '                       </a>' +
                        '                   </div>' +
                        '                   <div class="awe-media-body center margin-vertical-20">' +
                        '                       <a href="#" title="'+res[i].name+'">'+res[i].name+'</a>' +
                        '                   </div>' +
                        '               </div>' +
                        '           </div>';
                }

                $('.popular-categories-container').append(appendHtml);
                $('#popular-categories-container-row').removeClass('loading').addClass('loaded');

                if(res.length < 8 && paginate != 1) {
                    $('.popular-category-show-more-container').fadeOut(300, function () {
                        $('.popular-category-show-more-container').remove();
                    });
                }
                else {
                    $('.popular-category-show-more-container').html('<span class="popular-category-show-more" onclick="showMoreProductCategories();">More <i class="fa fa-angle-double-down"></i> </span>')
                        .addClass('loaded').removeClass('loading-ring');
                }


                setTimeout(function () {
                    $('.category-block').addClass('loaded');
                }, 200);

            }
        },
        error: function (xhr, textShort, errorThrown) {
            console.log(xhr);
            console.log(textShort);
            console.log(errorThrown);
        }
    });
}

function showMoreNewProducts() {
    var paginate = window.newProductPaginate;
    getNewProducts(paginate, 8);
    window.newProductPaginate++;
}

function getNewProducts(paginate, perPage) {
    if(paginate != 1) {
        $('.new-products-show-more-container').removeClass('loaded').addClass('loading-ring').html('');
    }
    //ajax method
    $.ajax({
        type: 'GET',
        url: window.domain + 'api/get-new-products',
        data: {paginate:paginate, perpage:perPage}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var appendHtml = '';
                var res = JSON.parse(data);
                var mainImgUrl = window.server+'assets/backend/images/products/products/default.png';
                var hoverImgUrl = '';
                var categoryBlockClass = '';

                for(var i = 0; i < res.length; i++) {
                    for(var j = 0; j < res[i].img.length; j++) {
                        if(res[i].img[j].type == 1) {
                            mainImgUrl = window.server+'assets/backend/images/products/products/'+res[i].img[j].imgid+'.'+res[i].img[j].ext;
                        }
                        else if(res[i].img[j].type == 2) {
                            hoverImgUrl = window.server+'assets/backend/images/products/products/'+res[i].img[j].imgid+'.'+res[i].img[j].ext;
                        }
                    }

                    if(paginate != 1) {
                        categoryBlockClass = 'new-products-block'
                    }

                    appendHtml += ' <div class="col-md-3 col-sm-4 col-xs-6">' +
                        '               <div id="product-block-'+res[i].id+'" class="product product-grid '+categoryBlockClass+'">' +
                        '                   <div class="product-media">' +
                        '                       <div class="product-thumbnail">' +
                        '                           <a href="'+window.domain+'product/details/'+res[i].id+'" title="'+res[i].name+'">' +
                        '                               <img src="'+mainImgUrl+'" alt="'+res[i].name+' Product Image" class="current">' +
                        '                           </a>' +
                        '                       </div>' +
                        '                       <!-- /.product-thumbnail -->' +
                        '                       <div class="product-hover">' +
                        '                           <div class="product-actions">' +
                        '                               <a href="#" class="awe-button product-add-cart" data-toggle="tooltip" title="Add to cart">' +
                        '                                   <i class="icon icon-shopping-bag"></i>' +
                        '                               </a>' +
                        '                               <a href="#" class="awe-button product-quick-whistlist" data-toggle="tooltip" title="Add to whistlist">' +
                        '                                   <i class="glyphicon glyphicon-heart"></i>' +
                        '                               </a>' +
                        '                               <a href="javascript:void(0);" onclick="quickView('+res[i].id+');" class="awe-button product-quick-view" data-toggle="tooltip" title="Quickview">' +
                        '                                   <i class="icon icon-eye"></i>' +
                        '                               </a>' +
                        '                           </div>' +
                        '                       </div>' +
                        '                       <!-- /.product-hover -->' +
                        '                       <span class="product-label new">' +
                        '                           <span>new</span>' +
                        '                       </span>' +
                        '                   </div>' +
                        '                   <!-- /.product-media -->' +
                        '                   <div class="product-body">' +
                        '                       <h2 class="product-name">' +
                        '                           <a href="'+window.domain+'product/details/'+res[i].id+'" title="Ipsum Dolor Sit Hair Care">'+res[i].name+'</a>' +
                        '                       </h2>' +
                        '                       <!-- /.product-product -->' +
                        '                       <div class="product-category">' +
                        '                           <span>'+res[i].sub_category+'</span>' +
                        '                       </div>' +
                        '                       <!-- /.product-category -->' +
                        '                       <div class="product-price">' +
                        '                           <span class="amount">$'+(res[i].price)+'</span>' +
                        '                       </div>' +
                        '                       <!-- /.product-price -->' +
                        '                   </div>' +
                        '                   <!-- /.product-body -->' +
                        '               </div>' +
                        '               <!-- /.product -->' +
                        '           </div>';
                }

                $('.new-products-container').append(appendHtml);
                $('#new-products-container-row').removeClass('loading').addClass('loaded');

                if(res.length < 8 && paginate != 1) {
                    $('.new-products-show-more-container').fadeOut(300, function () {
                        $('.new-products-show-more-container').remove();
                    });
                }
                else {
                    $('.new-products-show-more-container').html('<span class="new-products-show-more" onclick="showMoreNewProducts();">More <i class="fa fa-angle-double-down"></i> </span>')
                        .addClass('loaded').removeClass('loading-ring');
                }


                setTimeout(function () {
                    $('.new-products-block').addClass('loaded');
                }, 200);

            }
        },
        error: function (xhr, textShort, errorThrown) {
            console.log(xhr);
            console.log(textShort);
            console.log(errorThrown);
        }
    });
}



function showMoreFeaturedProducts() {
    var paginate = window.featuredProductPaginate;
    getFeaturedProducts(paginate, 12);
    window.featuredProductPaginate++;
}

function getFeaturedProducts(paginate, perPage) {
    if(paginate != 1) {
        $('.featured-products-show-more-container').removeClass('loaded').addClass('loading-ring').html('');
    }
    //ajax method
    $.ajax({
        type: 'GET',
        url: window.domain + 'api/get-featured-products',
        data: {paginate:paginate, perpage:perPage}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var appendHtml = '';
                var res = JSON.parse(data);
                var mainImgUrl = window.server+'assets/backend/images/products/products/default.png';

                for(var i = 0; i < res.length; i++) {
                    var hoverImgUrl, categoryBlockClass, label = '';

                    for(var j = 0; j < res[i].img.length; j++) {
                        if(res[i].img[j].type == 1) {
                            mainImgUrl = window.server+'assets/backend/images/products/products/'+res[i].img[j].imgid+'.'+res[i].img[j].ext;
                        }
                        else if(res[i].img[j].type == 2) {
                            hoverImgUrl = window.server+'assets/backend/images/products/products/'+res[i].img[j].imgid+'.'+res[i].img[j].ext;
                        }
                    }

                    if(paginate != 1) {
                        categoryBlockClass = 'featured-products-block'
                    }

                    if(res[i].new == 2) {
                        label ='<span class="product-label new">' +
                            '       <span>new</span>' +
                            '   </span>';
                    }

                    appendHtml += ' <div class="col-md-3 col-sm-4 col-xs-6">' +
                        '               <div id="product-block-'+res[i].id+'" class="product product-grid '+categoryBlockClass+'">' +
                        '                   <div class="product-media">' +
                        '                       <div class="product-thumbnail">' +
                        '                           <a href="'+window.domain+'product/details/'+res[i].id+'" title="'+res[i].name+'">' +
                        '                               <img src="'+mainImgUrl+'" alt="'+res[i].name+' Product Image" class="current">' +
                        '                           </a>' +
                        '                       </div>' +
                        '                       <!-- /.product-thumbnail -->' +
                        '                       <div class="product-hover">' +
                        '                           <div class="product-actions">' +
                        '                               <a href="#" class="awe-button product-add-cart" data-toggle="tooltip" title="Add to cart">' +
                        '                                   <i class="icon icon-shopping-bag"></i>' +
                        '                               </a>' +
                        '                               <a href="#" class="awe-button product-quick-whistlist" data-toggle="tooltip" title="Add to whistlist">' +
                        '                                   <i class="glyphicon glyphicon-heart"></i>' +
                        '                               </a>' +
                        '                               <a href="javascript:void(0);" onclick="quickView('+res[i].id+');" class="awe-button product-quick-view" data-toggle="tooltip" title="Quickview">' +
                        '                                   <i class="icon icon-eye"></i>' +
                        '                               </a>' +
                        '                           </div>' +
                        '                       </div>' +
                        '                       <!-- /.product-hover -->' +
                        '                       ' +label+
                        '                   </div>' +
                        '                   <!-- /.product-media -->' +
                        '                   <div class="product-body">' +
                        '                       <h2 class="product-name">' +
                        '                           <a href="'+window.domain+'product/details/'+res[i].id+'" title="'+res[i].name+'">'+res[i].name+'</a>' +
                        '                       </h2>' +
                        '                       <!-- /.product-product -->' +
                        '                       <div class="product-category">' +
                        '                           <span>'+res[i].sub_category+'</span>' +
                        '                       </div>' +
                        '                       <!-- /.product-category -->' +
                        '                       <div class="product-price">' +
                        '                           <span class="amount">$'+res[i].price+'</span>' +
                        '                       </div>' +
                        '                       <!-- /.product-price -->' +
                        '                   </div>' +
                        '                   <!-- /.product-body -->' +
                        '               </div>' +
                        '               <!-- /.product -->' +
                        '           </div>';
                }

                $('.featured-products-container').append(appendHtml);
                $('#featured-products-container-row').removeClass('loading').addClass('loaded');

                if(res.length < 8 && paginate != 1) {
                    $('.featured-products-show-more-container').fadeOut(300, function () {
                        $('.featured-products-show-more-container').remove();
                    });
                }
                else {
                    $('.featured-products-show-more-container').html('<span class="featured-products-show-more" onclick="showMoreFeaturedProducts();">More <i class="fa fa-angle-double-down"></i> </span>')
                        .addClass('loaded').removeClass('loading-ring');
                }


                setTimeout(function () {
                    $('.featured-products-block').addClass('loaded');
                }, 200);

            }
        },
        error: function (xhr, textShort, errorThrown) {
            console.log(xhr);
            console.log(textShort);
            console.log(errorThrown);
        }
    });
}